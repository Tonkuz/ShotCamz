//
//  AppViewController.m
//  CustomTabBar
//
//  Created by Brian Frohlich on 10/4/15.
//  Copyright © 2015. All rights reserved.
//

#import "AppViewController.h"

#import "Barcode.h"

@import AVFoundation;   // iOS7 only import style


@interface AppViewController ()
- (void) appBecameActive:(NSNotification *) notification;
@property (strong, nonatomic) NSMutableArray * foundBarcodes;
@property (nonatomic, strong) UIPopoverController *popOver;
@end

@implementation AppViewController{
    // for rfid bar code scanner
    AVCaptureSession *_captureSession;
    AVCaptureDevice *_videoDevice;
    AVCaptureDeviceInput *_videoInput;
    AVCaptureVideoPreviewLayer *_previewLayer;
    BOOL _running;
    AVCaptureMetadataOutput *_metadataOutput;
}

@synthesize ConfigurationText, SaveButtonRef,DelayText, LengthText, Waiting, CancelButtonRef, StartPreviewButtonRef, SelectCameraRef, CameraSelectLabelRef;

@synthesize inputStream, outputStream;

@synthesize Preview1Ref;
@synthesize VideosAvailableRef, VideosDownloadedRef, DeleteAllButtonRef,StartButtonRef, DownloadTextRef, DeleteAllFromServerRef, CancelScanButton, ScanBackground,PictureView;

static  NSMutableArray *AvailableVideos;  
static int filesDownloaded;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.session = [self backgroundSession];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    LengthText.delegate = self;
    DelayText.delegate = self;
    RFIDTag.delegate = self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    myRFIDTag = [defaults stringForKey:@"LastRFIDTag"];
    RFIDTag.text = myRFIDTag;
    
    _previewView.hidden = true;
    CancelScanButton.hidden = true;
    
    [self setupCaptureSession];
    _previewLayer.frame = _previewView.bounds;
    [_previewView.layer addSublayer:_previewLayer];
    _foundBarcodes = [[NSMutableArray alloc] init];
    
//    // listen for going into the background and stop the session
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(applicationWillEnterForeground:)
//     name:UIApplicationWillEnterForegroundNotification
//     object:nil];
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(applicationDidEnterBackground:)
//     name:UIApplicationDidEnterBackgroundNotification
//     object:nil];
    
    // set default allowed barcode types, remove types via setings menu if you don't want them to be able to be scanned
    _allowedBarcodeTypes = [NSMutableArray new];
    [self.allowedBarcodeTypes addObject:@"org.iso.QRCode"];
    [self.allowedBarcodeTypes addObject:@"org.iso.PDF417"];
    [self.allowedBarcodeTypes addObject:@"org.gs1.UPC-E"];
    [self.allowedBarcodeTypes addObject:@"org.iso.Aztec"];
    [self.allowedBarcodeTypes addObject:@"org.iso.Code39"];
    [self.allowedBarcodeTypes addObject:@"org.iso.Code39Mod43"];
    [self.allowedBarcodeTypes addObject:@"org.gs1.EAN-13"];
    [self.allowedBarcodeTypes addObject:@"org.gs1.EAN-8"];
    [self.allowedBarcodeTypes addObject:@"com.intermec.Code93"];
    [self.allowedBarcodeTypes addObject:@"org.iso.Code128"];

}


- (void) viewDidUnload
{
    RFIDTag = nil;
    [super viewDidUnload];
}


// When the app launches or is foregrounded, this will get called via NSNotification
// to warm up the camera.
- (void) appBecameActive:(NSNotification *) notification
{
}

-(void)dismissKeyboard {
    [LengthText resignFirstResponder];
    [DelayText resignFirstResponder];
    myRFIDTag = RFIDTag.text;
    [RFIDTag resignFirstResponder];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self getConfiguration];
    [self getVideoList];
}




- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [connection1_stream cancel];
    [connection1 cancel];

    // save values
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:myRFIDTag forKey:@"LastRFIDTag"];
    [defaults synchronize];
}

// used to save configuration data or send delete all message to server.
-(void)SendViaTCP:(NSData*)data
{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"192.168.178.121", 5000, &readStream, &writeStream);
    
    inputStream = (NSInputStream *)CFBridgingRelease(readStream);
    outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
    [outputStream write:[data bytes] maxLength:[data length]];
}



#pragma NSStream delegate functions

// TCP code for reading/writing configuration data.

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream opened");
            break;
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStream) {
                
                uint8_t buffer[1024];
                int len;
                
                while ([inputStream hasBytesAvailable]) {
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        
                        if (nil != output) {
                            NSLog(@"server said: %@", output);
                            ConfigurationText.text = output;
                        }
                    }
                }
            }
            break;
            
        case NSStreamEventErrorOccurred:
            
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            theStream = nil;
            
            break;
        default:
            NSLog(@"Unknown event");
    }
}



// Preview code
// Start and stop camera.
// Put together jpegs from camera stream and show in window.

- (void)SendCommandToCamera:(int)camera{
    
    // Post JSON code to url
    NSError *error;
    
    NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    NSString * sCamera = SONY_CAMERA_1;
    if(camera == 1)
    {
        sCamera = SONY_CAMERA_1;
        yourURL = [[NSURL alloc] initWithString:SONY_CAMERA_1_GET];
   }
    else{
        sCamera = SONY_CAMERA_2;
        yourURL = [[NSURL alloc] initWithString:SONY_CAMERA_2_GET];
    }

    NSURL *myURL = [[NSURL alloc] initWithString:sCamera];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:myURL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //Set request to post
    [request setHTTPMethod:@"POST"];
    
    //Set content type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // set data
    NSData *postData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&error];
    [request setHTTPBody:postData];
    
    // create connection and set delegate if needed
        connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self
                                              startImmediately:YES];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(connection==connection1)
    {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:yourURL
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        //Set request to post
        [request setHTTPMethod:@"GET"];
        
        //Set content type
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        connection1_stream = [[NSURLConnection alloc] initWithRequest:request delegate:self
                                                    startImmediately:YES];
        [connection1 cancel];
    }
    else if(connection==connection1_stream)
    {
#define SONY_LIVEVIEW_HEADER_SIZE   136
        uint8_t startByte, payloadType;
        NSRange r = {0, 1};
        [data getBytes:&startByte range:r];
        [data getBytes:&payloadType range:NSMakeRange(1, 1)];
        if(startByte == 0xff && payloadType == 1)
        {
            uint8_t payloadsize[4];
            [data getBytes:&payloadsize[1] range:NSMakeRange(12, 3)];
            payloadsize[0] = 0;
            
            sizeOfJPEG_1 = (payloadsize[1] << 16) +
            (payloadsize[2] << 8) + payloadsize[3];
            
            uint8_t payloadPadSize[1];
            [data getBytes:&payloadPadSize[0] range:NSMakeRange(15, 1)];
            sizeOfJPEGPad_1 = payloadPadSize[0];
            jpegBytes_1 = [[NSMutableData alloc] initWithLength:0];
            
            if([data length] > SONY_LIVEVIEW_HEADER_SIZE)
            {
                [jpegBytes_1 appendData:[data subdataWithRange:NSMakeRange(SONY_LIVEVIEW_HEADER_SIZE, [data length] - SONY_LIVEVIEW_HEADER_SIZE)]];
            }
        }
        else
            [jpegBytes_1 appendData:data];
        
        
        if([jpegBytes_1 length] >= sizeOfJPEG_1)
        {
            // pad with garbage
            [jpegBytes_1 appendData:[data subdataWithRange:NSMakeRange(0, sizeOfJPEGPad_1)]];
            
            UIImage *myImageObj = [[UIImage alloc]initWithData:jpegBytes_1];
            
            Preview1Ref.image = myImageObj;
        }
    }// if stream 1
}




#pragma mark - IBAction

- (IBAction)SelectCamera:(id)sender {
    
    [self StopLiveView];
    [self StartLiveView];
}

- (IBAction)StartPreview:(id)sender {
    // toggle button
    StartPreviewButtonRef.selected = ![StartPreviewButtonRef isSelected];
    
    if(StartPreviewButtonRef.selected)
    {
        Preview1Ref.hidden = false;
        CameraSelectLabelRef.hidden = false;
        SelectCameraRef.hidden = false;
        [self StartLiveView];
    }
    else
    {
        Preview1Ref.hidden = true;
        CameraSelectLabelRef.hidden = true;
        SelectCameraRef.hidden = true;
        [self StopLiveView];
    }
}


//Save new configuration data
- (IBAction)Save:(id)sender {
    SaveButtonRef.enabled = false;
    CancelButtonRef.enabled = false;
    
    // Check for valid data
    int before = [LengthText.text intValue];
    int after = [DelayText.text intValue];
    if(before == 0 || after == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"SCS - Invalid Configuration Data"
                                              message:@"Value must be > 0"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES
                                                                           completion:NULL];
                                       [self getConfiguration];
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    NSString *length = [[NSString alloc]initWithFormat:@"%i", (before + after)];
    [configArray enumerateObjectsUsingBlock:^(id s, NSUInteger idx, BOOL *stop) {
        NSRange r = [s rangeOfString:@"ClipLength="];
        if(r.length > 0)
        {
            NSString *ss = [[NSString alloc] initWithFormat:@"ClipLength=%@", length];
            [configArray replaceObjectAtIndex:idx withObject:ss];
        }
        
        r = [s rangeOfString:@"KillDelay="];
        if(r.length > 0)
        {
            NSString *ss = [[NSString alloc] initWithFormat:@"KillDelay=%@", DelayText.text];
            [configArray replaceObjectAtIndex:idx withObject:ss];
        }
    }];
    
    NSString *sConfig = [configArray componentsJoinedByString:@"\n"];
    NSData *data = [[NSData alloc] initWithData:[sConfig dataUsingEncoding:NSASCIIStringEncoding]];
    
    [self SendViaTCP:data]; // Send data to SCS via tcp.
}

// delete all videos from server.
- (IBAction)DeleteAll:(id)sender {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"SCS"
                                          message:@"Are you sure you want to erase all videos from the server?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSData *data = [[NSData alloc] initWithData:[@"DeleteAll" dataUsingEncoding:NSASCIIStringEncoding]];
                                   [self SendViaTCP:data]; // Send data to SCS via tcp.
                                   DeleteAllFromServerRef.enabled = false;
                                   // erase local file
                                   NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                   NSString *documentsDirectory = [paths objectAtIndex:0];
                                   NSString *s = [documentsDirectory stringByAppendingPathComponent:MyVideoLogFile];
                                   if(true == [[NSFileManager defaultManager] fileExistsAtPath:s])
                                       [[NSFileManager defaultManager] removeItemAtPath:s error:nil];
                                   DeleteAllButtonRef.enabled = false;
                               }];
    [alertController addAction:okAction];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES
                                                                           completion:NULL];
 
                                   }];
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


// cancel change parameters
- (IBAction)Cancel:(id)sender {
    SaveButtonRef.enabled = false;
    CancelButtonRef.enabled = false;
    [self getConfiguration];
}

- (IBAction)Changed:(id)sender {
    SaveButtonRef.enabled = true;
    CancelButtonRef.enabled = true;
}



// This button initiates a scan session to scan a single barcode. The session will exit
// as soon as something is found.
- (IBAction) scanButtonPressed
{
    _previewView.hidden = false;
    CancelScanButton.hidden = false;
    ScanBackground.hidden = false;

    [self startRunning];
}

- (IBAction)Refresh:(id)sender {
    VideosDownloadedRef.text = @""; // zero out at start.
    [self getVideoList];
    [self getConfiguration];
}

- (IBAction)getLastTag:(id)sender {
    VideosDownloadedRef.text = @""; // zero out at start.

    NSString *link = [[NSString alloc]initWithFormat:@"http://%@%@", MyServerAddress, LastRFIDTag];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:link] cachePolicy:0 timeoutInterval:5];
    NSURLResponse* response=nil;
    NSError* error=nil;
    NSData* data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* myStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if(error != nil)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"SCS Error"
                                              message:[error localizedDescription]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES
                                                                           completion:NULL];                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        RFIDTag.text = [myStr stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        myRFIDTag = RFIDTag.text;
        
        [self getVideoList];
    }
}


#pragma mark - Download videos


- (IBAction)StartDownloading:(id)sender {
    StartButtonRef.enabled = false;
    filesDownloaded = 0;
    timr = [NSTimer scheduledTimerWithTimeInterval:(float).1 target:self selector:@selector(GetURLTimer:) userInfo:nil repeats:YES];
    
    NSString *stringURL = [NSString stringWithFormat:@"http://%@/%@", MyServerAddress, AvailableVideos[0]];
    NSURL *downloadURL = [NSURL URLWithString:stringURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:downloadURL];
    NSURLSessionDownloadTask *downloadTask = [self.session downloadTaskWithRequest:request];
    [downloadTask resume];
}

- (IBAction)CancelDownloads:(id)sender {
    [timr invalidate];
    timr = nil;
    
}
- (NSURLSession *)backgroundSession
{
    /*
     Using dispatch_once here ensures that multiple background sessions with the same identifier are not created in this instance of the application. If you want to support multiple background sessions within a single process, you should create each session with its own identifier.
     */
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.example.apple-samplecode.SimpleBackgroundTransfer.BackgroundSession"];
        session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    });
    return session;
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    // no such url?
    // need alert
    if(error != nil)
        [self ErrorOnDownloadOrSave];
}

//<##>Video Storage
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)downloadURL
{
    filesDownloaded++;
    
    //get the videoURL
    NSString *tempFilePath = [downloadURL path];
    NSError * error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:DocumentVideoDirectory];
    //NSString *newPath = [[tempFilePath stringByDeletingLastPathComponent] stringByAppendingPathComponent:AvailableVideos[0]];
    NSString *newPath = [dataPath stringByAppendingPathComponent:AvailableVideos[0]];
    NSFileManager *fileManager = [NSFileManager defaultManager];

    if (![fileManager fileExistsAtPath:dataPath])
        [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    if ([fileManager fileExistsAtPath:newPath] == YES)
        [fileManager removeItemAtPath:newPath error:&error];
    
    
    [fileManager copyItemAtPath:tempFilePath toPath:newPath error:&error];
    [self videoSaved2:newPath error:error contextInfo:(__bridge void *)(AvailableVideos[0])];
    
    /*if ( UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(newPath))
        // Copy it to the camera roll.
        UISaveVideoAtPathToSavedPhotosAlbum(newPath, self, @selector(videoSaved:didFinishSavingWithError:contextInfo:), (__bridge void *)(AvailableVideos[0]));
    
    else
    {
        [self ErrorOnDownloadOrSave];
        return;
//        NSString *s = [[NSString alloc]initWithFormat:@"Can't save: %@ to camera roll.\n\r", AvailableVideos[0]];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            DownloadTextRef.text = [DownloadTextRef.text stringByAppendingString:s];
//        });
    }*/
    
    [fileManager removeItemAtURL:downloadURL error:&error];
    
    if (error != nil)
    {
        // need alert
        NSString *s = [[NSString alloc]initWithFormat:@"Error: %@\n\r", error];
        dispatch_async(dispatch_get_main_queue(), ^{
            DownloadTextRef.text = [DownloadTextRef.text stringByAppendingString:s];
        });
    }
}

#pragma mark - Download tasks progress callback
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    
}

-(void)videoSaved2:(NSString *)videoPath error:(NSError *)error contextInfo:(void *)contextInfo{
    
    //[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error]
    //[self videoSaved videoPath:videoPath didFinishSavingWithError:error contextInfo: NSNull];
    [self videoSaved:videoPath didFinishSavingWithError:error contextInfo:contextInfo];
}
#pragma mark - Video saved to camera roll

-(void)videoSaved:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    NSString *s;
    
    if(error == nil)
    {
        s = [[NSString alloc]initWithFormat:@"Downloaded video: %@\n\r", contextInfo];
        [self appendToLogFile:(__bridge NSString *)(contextInfo)];
        
        [AvailableVideos removeObjectAtIndex:0];
        
        // Keep going
        if([AvailableVideos count] > 0)
        {//  we have popped the videos to download from the array. AvailableVideos[0] is always the next one to download.
            NSString *stringURL = [NSString stringWithFormat:@"http://%@/%@", MyServerAddress, AvailableVideos[0]];
            NSURL *downloadURL = [NSURL URLWithString:stringURL];
            NSURLRequest *request = [NSURLRequest requestWithURL:downloadURL];
            NSURLSessionDownloadTask *downloadTask = [self.session downloadTaskWithRequest:request];
            [downloadTask resume];
        }
        else
        {   // all is well
            [timr invalidate];
            timr = nil;
            VideosAvailableRef.text = @"";
            // show user result.
            NSString* downloadedStr = [[NSString alloc]initWithFormat:@"%i videos have been downloaded to your Camera Roll", [VideosDownloadedRef.text integerValue]];
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"SCS"
                                                  message:downloadedStr
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [alertController dismissViewControllerAnimated:YES
                                                    completion:NULL];
                                           VideosDownloadedRef.text = @""; // zero out at start.
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else
    {
        s = [[NSString alloc]initWithFormat:@"Error: %@ downloading video: %@\n\r", error, contextInfo];
        // this is bad. best to stop all downloads and clear files from Server.
        NSString* downloadedStr = [[NSString alloc]initWithFormat:@"There has been a problem with this video. Please retake it."];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"SCS - Error"
                                              message:downloadedStr
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                        NSData *data = [[NSData alloc] initWithData:[@"DeleteAll" dataUsingEncoding:NSASCIIStringEncoding]];
                                       [self SendViaTCP:data]; // Send data to SCS via tcp.
                                       DeleteAllFromServerRef.enabled = false;
                                       // erase local file
                                       NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                       NSString *documentsDirectory = [paths objectAtIndex:0];
                                       NSString *s = [documentsDirectory stringByAppendingPathComponent:MyVideoLogFile];
                                       if(true == [[NSFileManager defaultManager] fileExistsAtPath:s])
                                           [[NSFileManager defaultManager] removeItemAtPath:s error:nil];
                                       DeleteAllButtonRef.enabled = false;
                                    VideosDownloadedRef.text = @""; // zero out at start.
                                       [alertController dismissViewControllerAnimated:YES
                                                                           completion:NULL];
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        DownloadTextRef.text = s;
    });
}

#pragma - Timer
//1sec timer
- (void) GetURLTimer:(NSTimer *)timer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        VideosDownloadedRef.text = [[NSString alloc] initWithFormat:@"%u", filesDownloaded];
    });
}




// delete all videos from iPad
- (IBAction)ResetDownloads:(id)sender {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"SCS"
                                          message:@"Are you sure you want to delete all videos you have downloaded?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                   NSString *documentsDirectory = [paths objectAtIndex:0];
                                   NSString *s = [documentsDirectory stringByAppendingPathComponent:MyVideoLogFile];
                                   if(true == [[NSFileManager defaultManager] fileExistsAtPath:s])
                                       [[NSFileManager defaultManager] removeItemAtPath:s error:nil];
                                   DeleteAllButtonRef.enabled = false;
                                   [self getVideoList]; // refresh
                                   [alertController dismissViewControllerAnimated:YES
                                                                       completion:NULL];
                               }];
    [alertController addAction:okAction];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES
                                                                           completion:NULL];
                                   }];
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

// add name of video downloaded. Use this list to compare against Server's list. Avoid downloading videos already retrieved.

-(void) appendToLogFile:(NSString*)addFileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *sVideoLog = [documentsDirectory stringByAppendingPathComponent:MyVideoLogFile];
    
    NSString *withLineEnd = [addFileName stringByAppendingString:@"\r\n"];
    BOOL fileExists = [[NSFileManager defaultManager]  fileExistsAtPath:sVideoLog];
    if(fileExists == false)
        [withLineEnd writeToFile:sVideoLog atomically:NO encoding:NSUTF8StringEncoding error:nil];
    else
    {
        NSString* sWholeFile = [[NSString alloc] initWithContentsOfFile:sVideoLog
                                                           usedEncoding:nil error:nil];
        sWholeFile = [sWholeFile stringByAppendingString:withLineEnd];
        [sWholeFile writeToFile:sVideoLog atomically:NO encoding:NSUTF8StringEncoding error:nil];
    }
}

#pragma mark - Get Video List

-(void) getVideoList{

    // Get the video list file.

    if(myRFIDTag == nil)
        return;
    
    [Waiting startAnimating];

    NSString *videoList = [[NSString alloc]initWithFormat:@"http://%@%@", MyServerAddress, MyVideoListFile];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:videoList] cachePolicy:0 timeoutInterval:5];
    NSURLResponse* response=nil;
    NSError* error=nil;
    NSData* data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    [Waiting stopAnimating];

    if(error != nil)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"SCS Error"
                                              message:[error localizedDescription]
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES
                                                                           completion:NULL];
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {   // Make an array of the available videos.
        NSString* myStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSArray *a = [myStr componentsSeparatedByString:@"\n"];
        if([a count] > 0)
            DeleteAllFromServerRef.enabled = true;
        
        // find our saved file with all previously downloaded videos.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *s = [documentsDirectory stringByAppendingPathComponent:MyVideoLogFile];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:s];
        NSString* contents = @"";
        if(fileExists == true)
        {
            contents = [[NSString alloc] initWithContentsOfFile:s usedEncoding:nil error:nil];
            DeleteAllButtonRef.enabled = true;
        }
        NSArray *PreviouslyDownloadedVideos;
        if([contents length] > 0)
            PreviouslyDownloadedVideos = [contents componentsSeparatedByString:@"\r\n"];
        
        // Make a new list of videos not already downloaded.
        AvailableVideos = [[NSMutableArray alloc] initWithArray:a];
        [AvailableVideos removeObject:@""];
        
        // Build list of URLS with our RFID Tag.
        [AvailableVideos enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            // now you can remove the object without affecting the enumeration
            if([obj rangeOfString:myRFIDTag].length == 0)
            {// did not find our tag in this url
                [AvailableVideos removeObjectAtIndex:idx];
            }
            else{
                // it's a video with our tag, but do we already have it.
                NSString *newName = [obj lastPathComponent];
                if((PreviouslyDownloadedVideos != nil) && [PreviouslyDownloadedVideos indexOfObject:newName] != NSNotFound)
                    [AvailableVideos removeObjectAtIndex:idx];
            }
        }];
        
        printf("Found %lu videos matching our tag and not already downloaded.\n", (unsigned long)[AvailableVideos count]);
        VideosAvailableRef.text = [@([AvailableVideos count]) stringValue];
        VideosDownloadedRef.text = @""; // zero out at start.

        if([AvailableVideos count] > 0)
        {
            StartButtonRef.enabled = true;
         }
    }
}

#pragma mark - Get Configuration
-(void) getConfiguration{
        [Waiting startAnimating];

        // Get the configuration file.
        NSString *link = [[NSString alloc]initWithFormat:@"http://%@%@", MyServerAddress, MyConfigurationFile];
        
        NSURLRequest* cRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:link] cachePolicy:0 timeoutInterval:5];
        NSURLResponse* cResponse=nil;
        NSError* cError=nil;
        NSData* cData=[NSURLConnection sendSynchronousRequest:cRequest returningResponse:&cResponse error:&cError];
        NSString* myConfStr = [[NSString alloc] initWithData:cData encoding:NSUTF8StringEncoding];
        
        [Waiting stopAnimating];
        
        if(cError != nil)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"SCS Error"
                                                  message:[cError localizedDescription]
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [alertController dismissViewControllerAnimated:YES
                                                                               completion:NULL];
                                       }];
            //
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else
        {
            ConfigurationText.text = myConfStr;
            configArray = [[myConfStr componentsSeparatedByString:@"\n"] mutableCopy];
            /*
             General],
             ClipLength=5,
             KillDelay=2,
             cam1_IP=192.168.178.13,
             cam1_PASSWORD=SU11fgxz,
             cam1_SSID=DIRECT-fuH5:HDR-AZ1,
             cam1_UUID=f0696ce5-a692-4222-ab39-87130139b82d,
             cam2_IP=192.168.178.14,
             cam2_PASSWORD=xXz8iHu7,
             cam2_SSID=DIRECT-dVH5:HDR-AZ1,
             cam2_UUID=55428397-db3f-4994-a5ac-1dadce71d98b,
             cam3_IP=,
             cam4_IP=,
             */
            NSString *delay, *length;
            for(NSString *s in configArray)
            {
                NSRange r = [s rangeOfString:@"ClipLength="];
                if(r.length > 0)
                {
                    length = [s substringFromIndex:r.length];
                    length = [length stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                r = [s rangeOfString:@"KillDelay="];
                if(r.length > 0)
                {
                    delay = [s substringFromIndex:r.length];
                    delay = [delay stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
            }
            
            DelayText.text = delay; // aka after
            int before = [length intValue] - [delay intValue];
            LengthText.text = [[NSString alloc]initWithFormat:@"%i", before];   // now displaying before time.
        }
    }



#pragma mark -  camera preview.
-(void) StartLiveView{
    
    // Start Camera.
    
    str = @"{"
    "\"method\" : \"startLiveview\","
    "\"params\" : [],"
    "\"id\" : 1,"
    "\"version\" : \"1.0\""
    "}";
    
    int camera = (SelectCameraRef.selectedSegmentIndex == 0) ? 1 : 2;
    [self SendCommandToCamera:camera];

}

-(void) StopLiveView{
    
    // Stop Camera.
    [connection1_stream cancel];
    
//    str = @"{"
//    "\"method\" : \"stopLiveview\","
//    "\"params\" : [],"
//    "\"id\" : 1,"
//    "\"version\" : \"1.0\""
//    "}";
//    
//    int camera = (SelectCameraRef.selectedSegmentIndex == 0) ? 2 : 1;
//    [self SendCommandToCamera:camera];
}



#pragma mark - AV capture methods for barcode scanner

- (void)setupCaptureSession {
    // 1
    if (_captureSession) return;
    // 2
    _videoDevice = [AVCaptureDevice
                    defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (!_videoDevice) {
        NSLog(@"No video camera on this device!");
        return;
    }
    // 3
    _captureSession = [[AVCaptureSession alloc] init];
    // 4
    _videoInput = [[AVCaptureDeviceInput alloc]
                   initWithDevice:_videoDevice error:nil];
    // 5
    if ([_captureSession canAddInput:_videoInput]) {
        [_captureSession addInput:_videoInput];
    }
    // 6
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc]
                     initWithSession:_captureSession];
    _previewLayer.videoGravity =
    AVLayerVideoGravityResizeAspectFill;
    
    
    // capture and process the metadata
    _metadataOutput = [[AVCaptureMetadataOutput alloc] init];
    dispatch_queue_t metadataQueue =
    dispatch_queue_create("com.1337labz.featurebuild.metadata", 0);
    [_metadataOutput setMetadataObjectsDelegate:self
                                          queue:metadataQueue];
    if ([_captureSession canAddOutput:_metadataOutput]) {
        [_captureSession addOutput:_metadataOutput];
    }
}

- (void)startRunning {
    if (_running) return;
    [_captureSession startRunning];
    _metadataOutput.metadataObjectTypes =
    _metadataOutput.availableMetadataObjectTypes;
    _running = YES;
}
- (void)stopRunning {
    if (!_running) return;
    [_captureSession stopRunning];
    _running = NO;
}

#pragma mark - Delegate functions

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection
{
    [metadataObjects
     enumerateObjectsUsingBlock:^(AVMetadataObject *obj,
                                  NSUInteger idx,
                                  BOOL *stop)
     {
         if ([obj isKindOfClass:
              [AVMetadataMachineReadableCodeObject class]])
         {
             // 3
             AVMetadataMachineReadableCodeObject *code =
             (AVMetadataMachineReadableCodeObject*)
             [_previewLayer transformedMetadataObjectForMetadataObject:obj];
             // 4
             Barcode * barcode = [Barcode processMetadataObject:code];
             
             for(NSString * strr in self.allowedBarcodeTypes){
                 if([barcode.getBarcodeType isEqualToString:strr]){
                     [self validBarcodeFound:barcode];
                     return;
                 }
             }
         }
     }];
}

- (void) validBarcodeFound:(Barcode *)barcode{
    [self stopRunning];
    
    RFIDTag.text = barcode.getBarcodeData;
    myRFIDTag = barcode.getBarcodeData;
    [self stopRunning];
    _previewView.hidden = true;
    CancelScanButton.hidden = true;
    ScanBackground.hidden = true;
}

- (void) showBarcodeAlert:(Barcode *)barcode{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Code to do in background processing
        NSString * alertMessage = @"You found a barcode with type ";
        alertMessage = [alertMessage stringByAppendingString:[barcode getBarcodeType]];
        //        alertMessage = [alertMessage stringByAppendingString:@" and data "];
        //        alertMessage = [alertMessage stringByAppendingString:[barcode getBarcodeData]];
        alertMessage = [alertMessage stringByAppendingString:@"\n\nBarcode added to array of "];
        alertMessage = [alertMessage stringByAppendingString:[NSString stringWithFormat:@"%lu",(unsigned long)[self.foundBarcodes count]-1]];
        alertMessage = [alertMessage stringByAppendingString:@" previously found barcodes."];
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Barcode Found!"
                                                          message:alertMessage
                                                         delegate:self
                                                cancelButtonTitle:@"Done"
                                                otherButtonTitles:@"Scan again",nil];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Code to update the UI/send notifications based on the results of the background processing
            [message show];
            
        });
    });
}
- (IBAction)CancelScan:(id)sender {
    
    _previewView.hidden = true;
    CancelScanButton.hidden = true;
    ScanBackground.hidden = true;
    [self startRunning];
}




#pragma mark - Text Field Delegates

// make text fields move out of the way of the keyboard.
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark - View Videos

// go to camera roll. ios 9 puts a return button in the top left corner.
- (IBAction)GoToCamRoll:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"photos-redirect:///"]];
}



// bring up a popover for summary view of camera roll.
- (IBAction)ViewVideos:(id)sender {
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePickerController.delegate = self;
    imagePickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:imagePickerController.sourceType];
    
    _popOver = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
    [_popOver setPopoverContentSize:CGSizeMake(1024, 1024)];
    
    [_popOver presentPopoverFromRect:CGRectZero
                             inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionAny
                           animated:YES];
    [_popOver setPopoverContentSize:CGSizeMake(700, 700)];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    viewController.contentSizeForViewInPopover = CGSizeMake(800, 800);
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.popOver dismissPopoverAnimated:false];
}


- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController
          willRepositionPopoverToRect:(inout CGRect *)rect
                               inView:(inout UIView * _Nonnull *)view
{
    
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    return YES;
}





#pragma mark - Handle all downloading and save errors
-(void)ErrorOnDownloadOrSave
{
    // clear any outstanding videos to download.
    [AvailableVideos removeAllObjects];

    NSString* errStr = [[NSString alloc]initWithFormat:@"There has been a problem with this video. Please retake it."];
    UIAlertController *alertController = [UIAlertController
      alertControllerWithTitle:@"SCS - Error"
      message:errStr
                                          
      preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"OK", @"OK action")
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction *action)
     {
         VideosDownloadedRef.text = @""; // zero out at start.
        NSData *data = [[NSData alloc] initWithData:[@"DeleteAll" dataUsingEncoding:NSASCIIStringEncoding]];
        [self SendViaTCP:data]; // Send data to SCS via tcp.
        DeleteAllFromServerRef.enabled = false;

        // erase local file
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *s = [documentsDirectory stringByAppendingPathComponent:MyVideoLogFile];
        if(true == [[NSFileManager defaultManager] fileExistsAtPath:s])
         [[NSFileManager defaultManager] removeItemAtPath:s error:nil];
        DeleteAllButtonRef.enabled = false;
                                }];
        VideosDownloadedRef.text = @""; // zero out at start.

        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];

}
@end
