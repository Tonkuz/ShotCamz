//
//  CoreUtility.swift
//  ShotCamz
//
//  Created by Brian Ogden on 7/21/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import UIKit
import AVFoundation

struct MediaUtility {
    static func interfaceOrientationToVideoOrientation(orientation: UIInterfaceOrientation) -> AVCaptureVideoOrientation {
        switch orientation {
        case .Portrait:
            return .Portrait
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        case .LandscapeLeft:
            return .LandscapeLeft
        case .LandscapeRight:
            return .LandscapeRight
        default:
            break
        }
        
        print("Warning - Didn't recognise interface orientation (\(orientation))")
        return .Portrait
    }
    
    static func printAVAssetWriterStatus(status: AVAssetWriterStatus) {
        switch status {
        case .Cancelled:
            print("Cancelled")
        case .Completed:
            print("Completed")
        case .Failed:
            print("Failed")
        case .Unknown:
            print("Unkown")
        case .Writing:
            print("Unknown")
        }
    }
    
    static func printOSStatus(status: OSStatus) {
        switch status {
        case kCMSampleBufferError_AllocationFailed:
            print("kCMSampleBufferError_AllocationFailed")
        case kCMSampleBufferError_RequiredParameterMissing:
            print("kCMSampleBufferError_RequiredParameterMissing")
        case kCMSampleBufferError_AlreadyHasDataBuffer:
            print("kCMSampleBufferError_AlreadyHasDataBuffer")
        case kCMSampleBufferError_BufferNotReady:
            print("kCMSampleBufferError_BufferNotReady")
        case kCMSampleBufferError_SampleIndexOutOfRange:
            print("kCMSampleBufferError_SampleIndexOutOfRange")
        case kCMSampleBufferError_BufferHasNoSampleSizes:
            print("kCMSampleBufferError_BufferHasNoSampleSizes")
        case kCMSampleBufferError_BufferHasNoSampleTimingInfo:
            print("kCMSampleBufferError_BufferHasNoSampleTimingInfo")
        case kCMSampleBufferError_ArrayTooSmall:
            print("kCMSampleBufferError_ArrayTooSmall")
        case kCMSampleBufferError_InvalidEntryCount:
            print("kCMSampleBufferError_InvalidEntryCount")
        case kCMSampleBufferError_CannotSubdivide:
            print("kCMSampleBufferError_CannotSubdivide")
        case kCMSampleBufferError_SampleTimingInfoInvalid:
            print("kCMSampleBufferError_SampleTimingInfoInvalid")
        case kCMSampleBufferError_InvalidMediaTypeForOperation:
            print("kCMSampleBufferError_InvalidMediaTypeForOperation")
        case kCMSampleBufferError_InvalidSampleData:
            print("kCMSampleBufferError_InvalidSampleData")
        case kCMSampleBufferError_InvalidMediaFormat:
            print("kCMSampleBufferError_InvalidMediaFormat")
        case kCMSampleBufferError_Invalidated:
            print("kCMSampleBufferError_Invalidated")
        case kCMSampleBufferError_DataFailed:
            print("kCMSampleBufferError_DataFailed")
        case kCMSampleBufferError_DataCanceled:
            print("kCMSampleBufferError_DataCanceled")
        case kCMFormatDescriptionError_InvalidParameter:
            print("kCMFormatDescriptionError_InvalidParameter")
        default:
            print("unknown OSStatus")
            break
        }
    }
}