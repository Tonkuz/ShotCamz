//
//  ConfigurationModel.swift
//  ShotCamz
//
//  Created by Brian Ogden on 7/15/16.
//  Copyright © 2016 SCS. All rights reserved.
//

struct ConfigurationModel {
/*
 [1] = 0x7a299510 "ClipLength=6"
 [2] = 0x7a297da0 "KillDelay=3"
 [3] = 0x7a296190 "cam1_IP=192.168.178.13"
 [4] = 0x7a2961b0 "cam1_PASSWORD=SU11fgxz"
 [5] = 0x7a299450 "cam1_SSID=DIRECT-fuH5:HDR-AZ1"
 [6] = 0x7a299480 "cam1_UUID=f0696ce5-a692-4222-ab39-87130139b82d"
 [7] = 0x7a299910 "cam2_IP=192.168.178.14"
 [8] = 0x7a299930 "cam2_PASSWORD=xXz8iHu7"
 [9] = 0x7a299340 "cam2_SSID=DIRECT-dVH5:HDR-AZ1"
 [10] = 0x7a299370 "cam2_UUID=55428397-db3f-4994-a5ac-1dadce71d98b"
 [11] = 0x7a2961d0 "cam3_IP="
 [12] = 0x7a26b390 "cam4_IP="
 [13] = 0x7a2993b0 "OnsetThreshold=2.4"
 [14] = 0x011aa5a8 ""
 */
    let ClipLength:String
    let KilDelay:String
    let OnsetThreshold:String
}
