//
//  Common.h
//  SmartCourse
//
//  Created by Brian Frohlich on 8/29/15.
//
//

#ifndef SmartCourse_Common_h
#define SmartCourse_Common_h

#define MyServerAddress @"192.168.178.121:8080"
#define MyTCPServerAddress @"192.168.178.121:5000"
#define MyConfigurationFile @"/SCS.conf"
#define MyVideoListFile @"/VideoList"
#define LastRFIDTag @"/LastRFIDTag"
#define MyVideoLogFile @"SmartVideoLog" // Local file with all previously loaded videos.
#define DocumentVideoDirectory @"/Videos"

#define SONY_CAMERA_1   @"http://192.168.178.13:8080/sony/camera"
#define SONY_CAMERA_2   @"http://192.168.178.14:8080/sony/camera"
#define SONY_CAMERA_3   @"http://192.168.178.15:8080/sony/camera"

#define SONY_CAMERA_1_GET @"http://192.168.178.13:8080//liveview/liveviewstream"
#define SONY_CAMERA_2_GET @"http://192.168.178.14:8080//liveview/liveviewstream"
#define SONY_CAMERA_3_GET @"http://192.168.178.15:8080//liveview/liveviewstream"


#endif
