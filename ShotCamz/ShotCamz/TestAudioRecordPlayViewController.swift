//
//  TestAudioSessionViewController.swift
//  ShotCamz
//
//  Created by Brian Ogden on 7/23/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import UIKit
import AVFoundation

class TestAudioRecordPlayViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate {

    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    
    let pathToSoundSample = FileUtility.getPathToAudioSampleFile()
    var soundRecorder:AVAudioRecorder!
    var soundPlayer: AVAudioPlayer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRecorder()
    }

    @IBAction func btnRecordTouchUpInside(sender: UIButton) {
        if (sender.titleLabel?.text == "Record"){
            soundRecorder.record()
            sender.setTitle("Stop", forState: .Normal)
            btnPlay.enabled = false
        } else {
            soundRecorder.stop()
            sender.setTitle("Record", forState: .Normal)
        }
    }
    
    @IBAction func btnPlayTouchUpInside(sender: UIButton) {
        if (sender.titleLabel?.text == "Play"){
            btnRecord.enabled = false
            sender.setTitle("Stop", forState: .Normal)
            preparePlayer()
            soundPlayer.play()
        } else {
            soundPlayer.stop()
            sender.setTitle("Play", forState: .Normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupRecorder() {
        
        //set the settings for recorder
        
        let recordSettings = [
            AVFormatIDKey: Int(kAudioFormatAppleLossless),
            AVEncoderAudioQualityKey : AVAudioQuality.Max.rawValue,
            AVEncoderBitRateKey : 320000,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey : 44100.0
        ]
        
        do {
            try soundRecorder = AVAudioRecorder(URL: NSURL(fileURLWithPath: pathToSoundSample), settings: recordSettings as! [String : AnyObject])
            soundRecorder.delegate = self
            soundRecorder.prepareToRecord()
        } catch {
            print("AvAudioRecorder error: \(error)")
        }
    }
    
    func preparePlayer() {
        
        do {
            try soundPlayer = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: pathToSoundSample))
            soundPlayer.delegate = self
            soundPlayer.prepareToPlay()
            soundPlayer.volume = 1.0
        } catch {
            print("error preparingPlayer: \(error)")
        }
    }
    
    
    func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder, error: NSError?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioRecorderDidFinishRecording(aRecorder: AVAudioRecorder, successfully flag: Bool) {
        print("audioRecorderDidFinishRecording:successfully:")
        btnPlay.enabled = true
        btnRecord.setTitle("Record", forState: .Normal)

    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        btnRecord.enabled = true
        btnPlay.setTitle("Play", forState: .Normal)
    }
    
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
