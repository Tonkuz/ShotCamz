//
//  AppSettingsViewController.swift
//  ShotCamz
//
//  Created by Brian Ogden on 6/18/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import UIKit
import Foundation

class AppSettingsViewController: UIViewController, NSURLConnectionDataDelegate, NSStreamDelegate {

    @IBOutlet weak var labelVideosAvailable: UILabel!
    @IBOutlet weak var txtVideosAvailable: UITextField!
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var switchPreview: UISwitch!
    @IBOutlet weak var segPreview: UISegmentedControl!
    @IBOutlet weak var txtClipBefore: UITextField!
    @IBOutlet weak var txtClipAfter: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtViewStatus: UITextView!
    @IBOutlet weak var btnRefreshAppCmdResponse: UIButton!
    @IBOutlet weak var stepperClipBefore: UIStepper!
    @IBOutlet weak var stepperClipAfter: UIStepper!
    @IBOutlet weak var txtOnsetThreshold: UITextField!
    @IBOutlet weak var stepperOnsetThreshold: UIStepper!

    var IsPreview:Bool = false
    var yourUrl:NSURL?
    var connection1:NSURLConnection?
    var connection1_stream:NSURLConnection?
    var str:NSString = ""
    var sizeOfJPEG_1:UInt32 = 0
    var sizeOfJPEGPad_1:UInt32 = 0
    var jpegBytes_1: NSMutableData?
    var configArray:ConfigurationModel?
    var inputStream:NSInputStream?
    var outputStream:NSOutputStream?
    var AvailableVideos:[String]?
    let serverUtility = ServerUtility()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        populateUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func switchPreviewValChanged(sender: UISwitch) {
        IsPreview = sender.on
        var camera:Camera = .Camera1
        
        if segPreview.selectedSegmentIndex == 1 {
            camera = .Camera2
        }
        
        if(IsPreview) {
            segPreview.hidden = false
            StartLiveView(camera)
        } else {
            segPreview.hidden = true
            segPreview.selectedSegmentIndex = 0
            StopLiveView()
            imgPreview.image = nil
        }
    }
    
    // MARK: - UI Action Outlets
    @IBAction func segPreviewValChanged(sender: UISegmentedControl) {
        StopLiveView()
        
        switch sender.selectedSegmentIndex {
            case 0:
                StartLiveView(.Camera1)
            case 1:
                StartLiveView(.Camera2)
            default:
                break;
        }
    }
    
    @IBAction func btnClearVideoLogTouchUpInside(sender: UIButton) {
        serverUtility.clearVideoLogFile()
    }
    
    @IBAction func btnDeleteOnServerTouchUpInside(sender: UIButton) {
        DeleteVideosOnServer()
    }
    
    @IBAction func btnRefreshTouchUpInside(sender: UIButton) {
        populateUI()
    }

    @IBAction func btnDeleteOnCamera(sender: UIButton) {
        DeleteVideosOnCamera()
    }
    
    @IBAction func btnSaveTouchUpInside(sender: UIButton) {
        //save settings
        btnSave.enabled = false
        
        if txtClipBefore.text == nil || txtClipAfter.text == nil {
            return
        }
        
        print("txtClipBefore.text: \(txtClipBefore.text!)")
        print("txtClipAfter.text: \(txtClipAfter.text!)")
        
        guard let before = Int(txtClipBefore.text!) else {
            print("validation failed for txtClipBefore")
            btnSave.enabled = true
            return
        }
        
        guard let after = Int(txtClipAfter.text!) else {
             print("validation failed for txtClipAfter")
            btnSave.enabled = true
            return
        }
        
        guard let onsetThreshold = Double(txtOnsetThreshold.text!) else {
            print("validation failed for txtOnsetThreshold")
            btnSave.enabled = true
            return
        }


        let length: String = String(format: "%i", (before + after))
        
        let configurationModel = ConfigurationModel(ClipLength: length, KilDelay: after.description, OnsetThreshold: onsetThreshold.description)
        
        serverUtility.saveConfiguration(configurationModel)
        
        btnSave.enabled = true

    }
    
    @IBAction func btnRefreshAppCmdResponseTouchUpInside(sender: UIButton) {
         self.txtViewStatus.text = serverUtility.getAppCommandResponse()
    }
    
    @IBAction func btnGetMovieCountOnCameraTouchUpInside(sender: UIButton) {
        serverUtility.getMovieCountOnCamera()
    }
    
    @IBAction func stepperClipBeforeValueChanged(sender: UIStepper) {
        txtClipBefore.text = Int(sender.value).description
    }
    
    @IBAction func stepperClipAfterValueChanged(sender: UIStepper) {
        txtClipAfter.text = Int(sender.value).description
    }
    
    @IBAction func stepperOnsetThresholdValueChanged(sender: UIStepper) {
        txtOnsetThreshold.text = sender.value.description
    }
    
    // MARK: - Private Helpers
    func populateUI() {
        txtViewStatus.text = ""
        
        configArray = serverUtility.getConfiguration()
        
        if configArray == nil {
            txtViewStatus.text = "No connection to the server/cube"
            return
        }

        AvailableVideos = serverUtility.getVideoList()
        
        if AvailableVideos == nil {
            txtVideosAvailable.text = "0"
        }else {
           txtVideosAvailable.text = "\(AvailableVideos!.count)"
        }
        
        txtClipAfter.text = configArray!.KilDelay
        let before: Int = Int(configArray!.ClipLength)! - Int(configArray!.KilDelay)!
        txtClipBefore.text = before.description
        txtOnsetThreshold.text = configArray!.OnsetThreshold
        stepperClipBefore.value = Double(before)
        stepperClipAfter.value = Double(configArray!.KilDelay)!
        stepperOnsetThreshold.value = Double(configArray!.OnsetThreshold)!
        
        // now displaying before time.
    }
    
    func DeleteVideosOnServer() {
        
        let deleteAlert = UIAlertController(title: "Delete on Server", message: "Are you sure you want to erase all videos from the server?", preferredStyle: UIAlertControllerStyle.Alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.serverUtility.videoCleanupServer()
            self.txtViewStatus.text = self.serverUtility.getAppCommandResponse()
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action: UIAlertAction!) in
            deleteAlert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        presentViewController(deleteAlert, animated: true, completion: nil)
    }
    
    func DeleteVideosOnCamera() {
        
        let deleteAlert = UIAlertController(title: "Delete on Camera", message: "Are you sure you want to erase all videos on the camera?", preferredStyle: UIAlertControllerStyle.Alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.serverUtility.videoCleanupCamera()
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action: UIAlertAction!) in
            deleteAlert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        presentViewController(deleteAlert, animated: true, completion: nil)
    }

    // MARK: - Direct Connection To Camera
    
    func StartLiveView(camera:Camera) {
        // Start Camera.
        str = "{" +
            "\"method\" : \"startLiveview\"," +
            "\"params\" : []," +
            "\"id\" : 1," +
            "\"version\" : \"1.0\"" +
        "}"
        
        self.SendCommandToCamera(camera)
    }
    
    func StopLiveView() {
        // Stop Camera.
        
        if connection1_stream != nil {
            connection1_stream!.cancel()
        }
        //    str = @"{"
        //    "\"method\" : \"stopLiveview\","
        //    "\"params\" : [],"
        //    "\"id\" : 1,"
        //    "\"version\" : \"1.0\""
        //    "}";
        //
        //    int camera = (SelectCameraRef.selectedSegmentIndex == 0) ? 2 : 1;
        //    [self SendCommandToCamera:camera];
    }
    
    func SendCommandToCamera(camera: Camera) {
        // Post JSON code to url
        let data: NSData = str.dataUsingEncoding(NSUTF8StringEncoding)!
        var json:AnyObject?
        var sCamera: String?
        
        
        do {
            try json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers)
        } catch {
            print("unserializeable!!")
            return
        }
        
        switch camera {
        case .Camera1:
            sCamera = GlobalSonyCameraIps.Camera1
            yourUrl = NSURL(string: GlobalSonyCameraIps.Camera1Get)
        case .Camera2:
            sCamera = GlobalSonyCameraIps.Camera2
            yourUrl = NSURL(string: GlobalSonyCameraIps.Camera2Get)
        }
        
        if sCamera == nil {
            print("camera 1 and 2 the only available cameras at this time!")
            return
        }
        
        guard let myURL: NSURL = NSURL(string: sCamera!) else {
            print("bad url")
            return
        }
        
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: myURL, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 60.0)
        //Set request to post
        request.HTTPMethod = "POST"
        //Set content type
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var postData: NSData?
        
        do {
            try postData = NSJSONSerialization.dataWithJSONObject(json!, options: NSJSONWritingOptions.PrettyPrinted)
        } catch {
            print("NSJosnSerialization Failed!")
            return
        }
        
        request.HTTPBody = postData
        // create connection and set delegate if needed
        connection1 = NSURLConnection(request: request, delegate: self, startImmediately: true)
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        if connection == connection1 {
            let request: NSMutableURLRequest = NSMutableURLRequest(URL: yourUrl!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 60.0)
            //Set request to post
            request.HTTPMethod = "GET"
            //Set content type
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            connection1_stream = NSURLConnection(request: request, delegate: self, startImmediately: true)
            connection1!.cancel()
        }
        else if connection == connection1_stream {
            var startByte: UInt8 = 0x00
            var payloadType: UInt8 = 0x00
            let r: NSRange = NSRange(location: 0, length: 1)//[0, 1]
            data.getBytes(&startByte, range: r)
            data.getBytes(&payloadType, range: NSMakeRange(1, 1))
            if startByte == 0xff && payloadType == 1 {
                var payloadsize = [UInt8](count: 4, repeatedValue: 0x00)
                data.getBytes(&payloadsize[1], range: NSMakeRange(12, 3))
                payloadsize[0] = 0
                sizeOfJPEG_1 = (UInt32(payloadsize[1]) << 16) + (UInt32(payloadsize[2]) << 8) + UInt32(payloadsize[3])
                var payloadPadSize = [UInt8](count: 1, repeatedValue: 0x00)
                data.getBytes(&payloadPadSize[0], range: NSMakeRange(15, 1))
                sizeOfJPEGPad_1 = UInt32(payloadPadSize[0])
                jpegBytes_1 = NSMutableData(length: 0)!
                if data.length > GlobalSonyCameraIps.HeaderSize {
                    jpegBytes_1!.appendData(data.subdataWithRange(NSMakeRange(GlobalSonyCameraIps.HeaderSize, data.length - GlobalSonyCameraIps.HeaderSize)))
                }
            } else {
                jpegBytes_1!.appendData(data)
                if UInt32(jpegBytes_1!.length) >= sizeOfJPEG_1 {
                    // pad with garbage
                    jpegBytes_1!.appendData(data.subdataWithRange(NSMakeRange(0, Int(sizeOfJPEGPad_1))))
                    let myImageObj: UIImage = UIImage(data: jpegBytes_1!)!
                    imgPreview.image = myImageObj
                }
            }
        }
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
