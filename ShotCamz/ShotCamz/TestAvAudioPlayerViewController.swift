//
//  TestAvAudioPlayerViewController.swift
//  ShotCamz
//
//  Created by Brian Ogden on 7/24/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import UIKit
import AVFoundation

class TestAvAudioPlayerViewController: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var btnPlay: UIButton!
    
    var soundPlayer: AVAudioPlayer!
    var sampleAudioPath = FileUtility.getPathToAudioSampleFile()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //audioPlayer.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnPlayTouchUpInside(sender: UIButton) {
        if (sender.titleLabel?.text == "Play"){
            sender.setTitle("Stop", forState: .Normal)
            preparePlayer()
            soundPlayer.play()
        } else {
            soundPlayer.stop()
            sender.setTitle("Play", forState: .Normal)
        }
    }
    
    func preparePlayer() {
        do {
            try soundPlayer = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: sampleAudioPath))
            soundPlayer.delegate = self
            soundPlayer.prepareToPlay()
            soundPlayer.volume = 1.0
        } catch {
            print("error preparingPlayer: \(error)")
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
