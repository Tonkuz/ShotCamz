//
//  Constants.swift
//  SCS
//
//  Created by Brian Ogden on 6/16/16.
//  Copyright © 2016 swiftiostutorials.com. All rights reserved.
//
import UIKit

struct GlobalVars {
    static var DocumentVideoDirectory = "Videos" //AppViewController.m uses the "Videos" string but Objective C cannot access Swift structs or Global Variables - Brian Ogden 6-16-2016
    static var MyTCPServerAddress = "192.168.178.121"
    static var MyServerAddress = "192.168.178.121:8080"
    static var MyConfigurationFile = "SCS.conf"
    static var MyVideoListFile = "VideoList"
    static var MyVideoLogFile = "SmartVideoLog"
    static var LastRFIDTag = "LastRFIDTag"
    static var AppCommandResponse = "AppCommandResponse"
    static var MySampleAudioFileNameAndExt = "sample.caf"
    static var DocumentAudioDirectory = "Audio"
}

struct CameraCommands {
    static var deleteAllOnCamera = "deleteAllOnCamera"
    static var getMovieCountOnCamera = "getMovieCountOnCamera"
}

struct GlobalUIColors {
    static var UIColorBackgroundTabBarMenu = UIColor(red: 31.0/255.0, green: 33.0/255.0, blue: 36.0/255.0, alpha: 1.0)
    static var UIColorDefautFontColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0)
}

struct GlobalSonyCameraIps {
    static var Camera1 = "http://192.168.178.13:8080/sony/camera"
    static var Camera2 = "http://192.168.178.14:8080/sony/camera"
    
    static var Camera1Get = "http://192.168.178.13:8080//liveview/liveviewstream"
    static var Camera2Get = "http://192.168.178.14:8080//liveview/liveviewstream"
    
    static var HeaderSize = 136
}

enum Camera {
    case Camera1
    case Camera2
}