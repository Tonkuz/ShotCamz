//
//  Camera.h
//  ShotCamz
//
//  Created by Tony Thomas on 26/08/2016.
//  Copyright © 2016 SCS. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AVFoundation;
@interface Camera : NSObject
@property (nonatomic) AVCaptureVideoOrientation previewImageOrientation;
@property (nonatomic) BOOL recordingStarted;
-(void)setupCameraWithCompletionHandler:(void(^)(BOOL, AVCaptureSession*)) handler;
-(void)startRecording;
@end
