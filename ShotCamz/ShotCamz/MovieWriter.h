//
//  MovieWriter.h
//  ShotCamz
//
//  Created by Tony Thomas on 27/08/2016.
//  Copyright © 2016 SCS. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AVFoundation;
@interface MovieWriter : NSObject
@property (nonatomic, strong)   NSString* currentFileName;
-(void)writeSample:(CMSampleBufferRef) sample;

@end
