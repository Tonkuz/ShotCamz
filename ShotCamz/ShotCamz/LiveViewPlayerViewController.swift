//
//  LiveViewPlayerViewController.swift
//  SCS
//
//  Created by Brian Ogden on 6/15/16.
//  Copyright © 2016 swiftiostutorials.com. All rights reserved.
//

import UIKit
//import MobilePlayer
import AVFoundation
import AVKit


class LiveViewPlayerViewController: AVPlayerViewController {

    var videoModel: VideoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        if videoModel != nil {
            //playLocalBundledVideo(videoModel!)
            //playAvPlayer()
            //AvPlayerViewController()
            //AvPlayerViewControllerLocalBundleVideo()
            playVideo()
        } else {
            print("no videoModel data sent to LiveViewPlayerViewController!")
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Landscape
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func playVideo() {
        let videoURL = videoModel!.VideoUrl
        let player = AVPlayer(URL: videoURL)
    
        self.player = player
        self.player!.play()
        //self.player!.rate = Float(0)
    }
    
    
    // MARK: - TestVideoPlayerFuncs
    /*
    func playVideoSelf() {
        guard let path = NSBundle.mainBundle().pathForResource("testVid1", ofType:"mp4") else {
            print("Invalid video path")
            return
        }
        
        print("path = \(path)")
        
        let videoURL = NSURL(fileURLWithPath: path)
        
        let player = AVPlayer(URL: videoURL)
        //let playerViewController = AVPlayerViewController()
        self.player = player
        self.player!.play()
    }
    
    func AvPlayerViewControllerLocalBundleVideo() {
        guard let path = NSBundle.mainBundle().pathForResource("testVid1", ofType:"mp4") else {
            print("Invalid video path")
            return
        }
        
        print("path = \(path)")
        
        let videoURL = NSURL(fileURLWithPath: path)

        let player = AVPlayer(URL: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.presentViewController(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func AvPlayerViewController() {
        let videoURL = videoModel!.VideoUrl
        let player = AVPlayer(URL: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.presentViewController(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func playAvPlayer(){
        var player:AVPlayer!
        let avPlayerLayer:AVPlayerLayer = AVPlayerLayer(player: player)
        avPlayerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(avPlayerLayer)
        let steamingURL:NSURL = (videoModel?.VideoUrl)!
        player = AVPlayer(URL: steamingURL)
        player.play()
    }
    
    func playLocalBundledVideo(videoModel: VideoModel) {
        
        let videoURL = videoModel.VideoUrl
        
        let playerVC = MobilePlayerViewController(contentURL: videoURL)
        playerVC.title = videoModel.VideoTitle
        //playerVC.activityItems = [videoURL] // Check the documentation for more information.
        self.view.addSubview(playerVC.view)
        //presentMoviePlayerViewControllerAnimated(playerVC)
    }*/


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
