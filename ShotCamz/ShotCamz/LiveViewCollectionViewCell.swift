//
//  LiveViewCollectionViewCell.swift
//  SCS
//
//  Created by Brian Ogden on 6/15/16.
//  Copyright © 2016 swiftiostutorials.com. All rights reserved.
//

import UIKit

class LiveViewCollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var videoThumbImageView: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnSelect.layer.borderColor = UIColor.whiteColor().CGColor
        btnSelect.layer.borderWidth = 1
        
    }

    @IBAction func btnSelectTouchUpInside(sender: UIButton) {
        toggleSelected()
    }
    
    func isCellSelected () -> Bool {
        return btnSelect.selected
    }
    
    func displaySelect(hidden: Bool){
        btnSelect.hidden = hidden
    }
    
    func toggleSelected() {
        setCellSelected(!btnSelect.selected)
    }
    
    func setCellSelected(selected: Bool) {
        btnSelect.selected = selected
        
        if btnSelect.selected {
            btnSelect.backgroundColor = UIColor.whiteColor()
        }else {
            btnSelect.backgroundColor = nil
        }
    }
    
}
