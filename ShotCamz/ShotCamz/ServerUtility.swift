//
//  ServerUtility.swift
//  ShotCamz
//
//  Created by Brian Ogden on 7/14/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import Foundation

class ServerUtility: NSObject, NSStreamDelegate {
    var inputStream:NSInputStream?
    var outputStream:NSOutputStream?
    var configArray:[NSString]?
    
    // MARK: - Static
    func getConfiguration() -> ConfigurationModel? {
        
        guard let configArray:[NSString]? = getConfiguration() else {
            return nil
        }
        
        if configArray == nil {
            return nil
        }
        
        /*
         General],
         ClipLength=5,
         KillDelay=2,
         cam1_IP=192.168.178.13,
         cam1_PASSWORD=SU11fgxz,
         cam1_SSID=DIRECT-fuH5:HDR-AZ1,
         cam1_UUID=f0696ce5-a692-4222-ab39-87130139b82d,
         cam2_IP=192.168.178.14,
         cam2_PASSWORD=xXz8iHu7,
         cam2_SSID=DIRECT-dVH5:HDR-AZ1,
         cam2_UUID=55428397-db3f-4994-a5ac-1dadce71d98b,
         cam3_IP=,
         cam4_IP=,
         */
        //NSString * delay, length
        var delay:String = ""
        var length:String = ""
        var onsetThreshold:String = ""
        
        for s: NSString in configArray! {
            var r: NSRange = s.rangeOfString("ClipLength=")
            if r.length > 0 {
                length = s.substringFromIndex(r.length)
                length = length.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            }
            
            r = s.rangeOfString("KillDelay=")
            if r.length > 0 {
                delay = s.substringFromIndex(r.length)
                delay = delay.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            }
            
            r = s.rangeOfString("OnsetThreshold=")
            if r.length > 0 {
                onsetThreshold = s.substringFromIndex(r.length)
                onsetThreshold = onsetThreshold.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            }
        }
        
        return ConfigurationModel(ClipLength: length, KilDelay: delay, OnsetThreshold: onsetThreshold)
    }
    
    func getLastTag() -> String? {
        return sendParseFileRequest(GlobalVars.LastRFIDTag)
    }
    
    func getAppCommandResponse() -> String? {
        return sendParseFileRequest(GlobalVars.AppCommandResponse)
    }
    
    func getVideoList() -> [String]? {
        let currentRFID = getLastTag()
        
        var availableVideos:[String]? = nil
        
        if currentRFID == nil {
            return availableVideos
        }
        
        //Waiting.startAnimating()
        let videoList = "http://\(GlobalVars.MyServerAddress)/\(GlobalVars.MyVideoListFile)"
        let request: NSURLRequest = NSURLRequest(URL: NSURL(string: videoList)!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 5)
        var response: NSURLResponse? = nil
        let error: NSError? = nil
        let data: NSData = try! NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        //Waiting.stopAnimating()
        if error != nil {
            //update status
        }
        else {
            // Make an array of the available videos.
            let myStr: String = String(data: data, encoding: NSUTF8StringEncoding)!
            //let a:[NSString]? = myStr.componentsSeparatedByString("\n")
            availableVideos = myStr.componentsSeparatedByString("\n")
            if availableVideos!.count > 0 {
                //DeleteAllFromServerRef.enabled = true
            }
            // find our saved file with all previously downloaded videos.
            var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
            let documentsDirectory: String = paths[0] as! String
            let s: String = NSURL(fileURLWithPath: documentsDirectory).URLByAppendingPathComponent(GlobalVars.MyVideoLogFile).path!
            let fileExists: Bool = NSFileManager.defaultManager().fileExistsAtPath(s)
            var contents: String = ""
            if fileExists == true {
                print("\(GlobalVars.MyVideoLogFile) exists")
                do {
                    contents = try String(contentsOfFile: s, usedEncoding: nil)
                }
                catch {
                }
                //DeleteAllButtonRef.enabled = true
            }
            var PreviouslyDownloadedVideos:[String]?
            
            if contents.characters.count > 0 {
                PreviouslyDownloadedVideos = contents.componentsSeparatedByString("\r\n")
                PreviouslyDownloadedVideos!.removeAtIndex(PreviouslyDownloadedVideos!.indexOf("")!)
            }
            
            availableVideos!.removeAtIndex(availableVideos!.indexOf("")!)
            
            // Make a new list of videos not already downloaded.
            availableVideos = availableVideos!.filter { $0.containsString(currentRFID!) } // did not find our tag in this url
            if PreviouslyDownloadedVideos != nil {
                availableVideos = availableVideos!.filter{ !PreviouslyDownloadedVideos!.contains($0) }
            }// it's a video with our tag, but do we already have it.
            
            
            print("Found \(availableVideos!.count) videos matching our tag and not already downloaded.")
            //VideosDownloadedRef.text = ""
            // zero out at start.
            if availableVideos!.count > 0 {
                //StartButtonRef.enabled = true
            }
        }
        
        return availableVideos
    }
    
    func clearVideoLogFile() {
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory: String = paths[0] as! String
        let sVideoLog: String = NSURL(fileURLWithPath: documentsDirectory).URLByAppendingPathComponent(GlobalVars.MyVideoLogFile).path!
        let fileExists: Bool = NSFileManager.defaultManager().fileExistsAtPath(sVideoLog)
        do {
            if fileExists {
                try NSFileManager.defaultManager().removeItemAtPath(sVideoLog)
            }
        }
        catch {
        }
    }

    func appendToVideoLogFile(addFileName: String) {
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory: String = paths[0] as! String
        let sVideoLog: String = NSURL(fileURLWithPath: documentsDirectory).URLByAppendingPathComponent(GlobalVars.MyVideoLogFile).path!
        let withLineEnd: String = addFileName.stringByAppendingString("\r\n")
        let fileExists: Bool = NSFileManager.defaultManager().fileExistsAtPath(sVideoLog)
        do {
            if fileExists == false {
                do {
                    try withLineEnd.writeToFile(sVideoLog, atomically: false, encoding: NSUTF8StringEncoding)
                }
                catch {
                }
            }
            else {
                var sWholeFile: String = try String(contentsOfFile: sVideoLog, usedEncoding: nil)
                sWholeFile = sWholeFile.stringByAppendingString(withLineEnd)
                do {
                    try sWholeFile.writeToFile(sVideoLog, atomically: false, encoding: NSUTF8StringEncoding)
                }
                catch {
                }
            }
        }
        catch {
        }
    }
    
    func saveConfiguration(configurationModel:ConfigurationModel) {
        if configArray == nil {
            configArray = getConfiguration()
        }
        
        for (index, value) in configArray!.enumerate() {
            var r: NSRange = value.rangeOfString("ClipLength=")
            if r.length > 0 {
                let ss: String = String(format: "ClipLength=%@", configurationModel.ClipLength)
                configArray![index] = ss
            }
            
            r = value.rangeOfString("KillDelay=")
            if r.length > 0 {
                let ss: String = String(format: "KillDelay=%@", String(configurationModel.KilDelay))
                configArray![index] = ss
            }
            
            r = value.rangeOfString("OnsetThreshold=")
            if r.length > 0 {
                let ss: String = String(format: "OnsetThreshold=%@", String(configurationModel.OnsetThreshold))
                configArray![index] = ss
            }
            
            
        }
        
        let nsArrayConfig = configArray! as NSArray
        
        saveConfiguration(nsArrayConfig)

    }
    
    func videoCleanupServer() {
        let data: NSData = NSData(data: "DeleteAll".dataUsingEncoding(NSASCIIStringEncoding)!)
        SendViaTCP(data)
        // Send data to SCS via tcp.
        // erase local file
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory: String = paths[0] as! String
        let s: String = NSURL(fileURLWithPath: documentsDirectory).URLByAppendingPathComponent(GlobalVars.MyVideoLogFile).path!
        if true == NSFileManager.defaultManager().fileExistsAtPath(s) {
            do {
                try NSFileManager.defaultManager().removeItemAtPath(s)
            }
            catch {
            }
        }
    }
    
    // MARK - Send Camera Commands
    
    func videoCleanupCamera() {
       sendCameraCommand(CameraCommands.deleteAllOnCamera)
    }
    
    func getMovieCountOnCamera() {
       sendCameraCommand(CameraCommands.getMovieCountOnCamera)
    }

    
    // MARK: - Private Helpers
    private func saveConfiguration(nsArrayConfig: NSArray) {
        let sConfig: String = nsArrayConfig.componentsJoinedByString("\n")
        let data: NSData = NSData(data: sConfig.dataUsingEncoding(NSASCIIStringEncoding)!)
        SendViaTCP(data)
    }

    
    private func getConfiguration() -> [NSString]? {
        // Get the configuration file.
        configArray = nil
        
        let link = "http://\(GlobalVars.MyServerAddress)/\(GlobalVars.MyConfigurationFile)"
        let cRequest: NSURLRequest = NSURLRequest(URL: NSURL(string: link)!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 5)
        var cResponse: NSURLResponse?
        var cData: NSData?
        
        do {
            try cData = NSURLConnection.sendSynchronousRequest(cRequest, returningResponse: &cResponse)
        } catch {
            print("\(#function) -> sync request falied, error: \(error)")
            return configArray;
        }
        
        let myConfStr = String(data: cData!, encoding: NSUTF8StringEncoding)!
        //Waiting.stopAnimating()
        //ConfigurationText.text = myConfStr
        configArray = myConfStr.componentsSeparatedByString("\n") as [NSString]
        
        return configArray
    }

    
    private func sendParseFileRequest(fileName: String) -> String? {
        var fileContents:String? = nil
        
        let link = "http://\(GlobalVars.MyServerAddress)/\(fileName)"
        let request: NSURLRequest = NSURLRequest(URL: NSURL(string: link)!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 5)
        var response: NSURLResponse? = nil
        var error: ErrorType? = nil
        var myStr:String?
        
        do {
            let data: NSData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            myStr = String(data: data, encoding: NSUTF8StringEncoding)!
        }
        catch let theError {
            error = theError
        }
        
        
        if error != nil {
            print("error: \(#function) - \(error.debugDescription)")
        }
        else {
            fileContents = myStr!.stringByTrimmingCharactersInSet(NSCharacterSet.newlineCharacterSet())
        }
        
        return fileContents

    }
    
    private func sendCameraCommand(cmd: String) {
        let apiCall = "appcmd:\(cmd)"
        let data: NSData = NSData(data: apiCall.dataUsingEncoding(NSASCIIStringEncoding)!)
        SendViaTCP(data)
    }
    
    private func SendViaTCP(data: NSData) {
        var readStream: Unmanaged<CFReadStreamRef>?
        var writeStream: Unmanaged<CFWriteStreamRef>?
        CFStreamCreatePairWithSocketToHost(nil, (GlobalVars.MyTCPServerAddress as CFString), 5000, &readStream, &writeStream)
        inputStream = readStream?.takeUnretainedValue() //(CFBridgingRelease(readStream) as! NSInputStream)
        outputStream = writeStream?.takeRetainedValue() //(CFBridgingRelease(writeStream) as! NSOutputStream)
        inputStream!.delegate = self
        outputStream!.delegate = self
        inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        inputStream!.open()
        outputStream!.open()
        
        outputStream!.write(UnsafePointer<UInt8>(data.bytes), maxLength: data.length)
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        print("\(#function) didReceiveData called")
    }

}