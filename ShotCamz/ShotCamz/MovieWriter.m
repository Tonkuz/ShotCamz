//
//  MovieWriter.m
//  ShotCamz
//
//  Created by Tony Thomas on 27/08/2016.
//  Copyright © 2016 SCS. All rights reserved.
//

#import "MovieWriter.h"
@interface MovieWriter()
///buffering time for each movie file segment without onset
@property(nonatomic) int movieClipDuration;
@property(nonatomic) int movieClipDurationAfterOnset;
@property(nonatomic, strong) AVAssetWriter* writer;
@property(nonatomic, strong) AVAssetWriter* writerPrevious;
@property(nonatomic, strong) AVAssetWriterInput* writerInput;
@property(nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor* pixelBufferAdaptor;
@property(nonatomic) CGSize videoSize;
@property(nonatomic) int frameRate;
@property(nonatomic) dispatch_queue_t writerQueue;
@property(nonatomic) uint64_t presentationTime;
@property(nonatomic) uint64_t presentationTimeInterval;
@property(nonatomic) uint64_t recordingSwapingTime;
@property(nonatomic) uint32_t timeScale;
@end
@implementation MovieWriter

- (instancetype)initWithMovieSize:(CGSize) size andFrameRate:(int) frameRate
{
    self = [super init];
    if (self) {
        self.movieClipDuration = 30;//seconds
        self.movieClipDurationAfterOnset = 5;//seconds
        self.videoSize = size;
        self.frameRate = frameRate;
        self.timeScale = 1000000000;
        self.presentationTimeInterval =  self.timeScale / self.frameRate;
        self.presentationTime = 0;
        self.recordingSwapingTime = self.presentationTimeInterval * self.movieClipDuration * self.frameRate;
        
        self.writerQueue = dispatch_queue_create("shotCamz.writer.queue", DISPATCH_QUEUE_SERIAL);
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onAubioOnset)
                                                     name:@"kAubioOnset"
                                                   object:nil];

    }
    return self;
}
//when the onset happens, the recording stop counter starts and will end in movieClipDurationAfterOnset
//the movie file will combine the previous file if necessary to get the entire event in a single file
-(void)onAubioOnset{
    self.recordingSwapingTime = self.presentationTime + self.presentationTimeInterval * self.movieClipDurationAfterOnset * self.frameRate;
}
-(void)writeSample:(CMSampleBufferRef) sample{
    
    dispatch_async(self.writerQueue, ^{
        
        if (self.presentationTime < self.recordingSwapingTime) {
            
            [self writeSmapleInternal: sample];
         }
        else{
            self.writerPrevious = self.writer;
            [self.writerPrevious finishWritingWithCompletionHandler:^{
                
            }];
            self.writer = [self getWriter];
            [self writeSmapleInternal: sample];
        }
    });
}
-(void)writeSmapleInternal: (CMSampleBufferRef)sample{
    
    BOOL ready = NO;
    do {
        
        ready = self.writerInput.readyForMoreMediaData;
        if (!ready) {
            usleep(100);
        }
        
    } while (!ready);
    
    CVPixelBufferRef pixel_buffer =  (CVPixelBufferRef)CMSampleBufferGetImageBuffer(sample);
    CMTime presentationTime = CMTimeMake(self.presentationTime, self.timeScale);
    if (![self.pixelBufferAdaptor appendPixelBuffer:pixel_buffer withPresentationTime: presentationTime])
        NSLog(@"Problem appending pixel buffer at time: %@", CFBridgingRelease(CMTimeCopyDescription(kCFAllocatorDefault, presentationTime)));
    self.presentationTime += self.presentationTimeInterval;
}
-(AVAssetWriter*)getWriter{
    
    NSString* file = [self getFileName];
    NSError *error = nil;
    AVAssetWriter* assetWriter;
    assetWriter = [[AVAssetWriter alloc] initWithURL:[NSURL fileURLWithPath:file] fileType:AVFileTypeQuickTimeMovie error:&error];
    if (error) {
        return nil;
    }
    assetWriter.movieFragmentInterval =   CMTimeMakeWithSeconds(1, 30);
    NSString* strPreset = nil;
    if (self.videoSize.width==1920||self.videoSize.height==1920) {
        strPreset = AVOutputSettingsPreset1920x1080;
    }
    else if (self.videoSize.width==1280||self.videoSize.height==1280) {
        strPreset = AVOutputSettingsPreset1280x720;
    }
    else if(self.videoSize.width==960||self.videoSize.height==960){
        strPreset = AVOutputSettingsPreset960x540;
    }
    else if(self.videoSize.width==640||self.videoSize.height==640) {
        strPreset = AVOutputSettingsPreset640x480;
    }
    AVOutputSettingsAssistant * assitant = [AVOutputSettingsAssistant outputSettingsAssistantWithPreset:strPreset];
    
    [assitant setSourceVideoAverageFrameDuration:CMTimeMake(1, self.frameRate)];
    [assitant setSourceVideoMinFrameDuration:CMTimeMake(1, self.frameRate)];
    
    NSMutableDictionary* dictionary  = [[assitant videoSettings] mutableCopy];
    [dictionary setValue:[NSNumber numberWithInteger:self.videoSize.width] forKey:AVVideoWidthKey];
    [dictionary setValue:[NSNumber numberWithInteger:self.videoSize.height] forKey:AVVideoHeightKey];
    [dictionary setValue:AVVideoScalingModeResizeAspectFill forKey:AVVideoScalingModeKey];
    self.writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:dictionary];
    self.writerInput.expectsMediaDataInRealTime = YES;
    int pixelFormat = kCVPixelFormatType_420YpCbCr8BiPlanarFullRange;
    NSDictionary *sourcePixelBufferAttributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithInt:pixelFormat], kCVPixelBufferPixelFormatTypeKey,
                                                           [NSNumber numberWithInt:self.videoSize.width], kCVPixelBufferWidthKey,
                                                           [NSNumber numberWithInt:self.videoSize.height], kCVPixelBufferHeightKey,
                                                           nil];
    
    
    self.pixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.writerInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];

    if ([assetWriter canAddInput:self.writerInput]) {
        [assetWriter addInput:self.writerInput];
    }
    if ([assetWriter startWriting]) {
        [assetWriter startSessionAtSourceTime:CMTimeMake( 0,self.timeScale)];
    }
    return  assetWriter;
}
-(NSString*)getFileName{
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* file = [NSString stringWithFormat:@"%@/", docDir];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger fileCount = [defaults integerForKey:@"kRecordedFileCount"];
    fileCount++;
    [defaults setInteger:fileCount forKey:@"kRecordedFileCount"];
    NSString * path = [NSString stringWithFormat:@"Shot_%d", (int)fileCount];

    return [NSString stringWithFormat:@"%@%@.mp4", file, path];
}
@end
