//
//  LiveViewViewController.swift
//  ShotCamz
//
//  Created by Brian Ogden on 6/18/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import UIKit
import AVFoundation

class LiveViewViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, NSURLSessionDelegate, NSURLSessionDownloadDelegate {

    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var btnSelect: UIButton!
    
    var timr:NSTimer?
    var filesDownloaded:Int = 0
    var AvailableVideos:[String]?
    var session:NSURLSession?
    let serverUtility = ServerUtility()

    var isSelectMode = false {
        didSet {
            onSelectModeChange()
        }
    }
    var videoCollection = [VideoModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        session = getSession()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.videoCollection = [VideoModel]()
        BuildVideoCollection()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return videoCollection.count
    }
    
    private let reuseIdentifier = "liveViewCollectionViewCell"
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let dequeuedCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)
        let cell = dequeuedCell as! LiveViewCollectionViewCell
        // Configure the cell
        
        cell.videoThumbImageView.image = videoCollection[indexPath.row].VideoThumbNail
        cell.displaySelect(!isSelectMode)
        
        //clear cell that might have a current state of selected
        if !isSelectMode {
            cell.setCellSelected(false)
        }
        
        return cell
    }
    
    // MARK: Action Outlets
    
    @IBAction func btnSelectTouchUpInside(sender: UIButton) {
        isSelectMode = true
    }
    
    @IBAction func btnCancelTouchUpInside(sender: UIButton) {
        isSelectMode = false
    }
    
    @IBAction func btnRefreshTouchUpInside(sender: UIButton) {
        GetVideosFromServer()
    }
    
    @IBAction func btnDeleteTouchUpInside(sender: UIButton) {
        print("btnDeleteTouchUpInside()")
        
        var anySelected = false
        
        for case let cell as LiveViewCollectionViewCell in collectionView!.visibleCells() {
            if cell.isCellSelected() {
                anySelected = true
                
                if let indexPath = collectionView!.indexPathForCell(cell) {
                    FileUtility.deleteFileByNsurl(videoCollection[indexPath.row].VideoUrl)
                }
            }
        }
        
        if anySelected {
            BuildVideoCollection()
            isSelectMode = false
        }
    }
    
    // MARK: Private Helpers
    private func GetVideosFromServer() {
        AvailableVideos = serverUtility.getVideoList()
        
        if(AvailableVideos == nil) {
            return
        }
        
        if(AvailableVideos!.count <= 0) {
            return
        }
        
        activity.startAnimating()
        
        filesDownloaded = 0
        timr = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(self.GetURLTimer), userInfo: nil, repeats: true)
        //timr = NSTimer.scheduledTimerWithTimeInterval(Float(0.1), target: self, selector: #selector(self.GetURLTimer), userInfo: nil, repeats: true)
        let stringURL: String = "http://\(GlobalVars.MyServerAddress)/\(AvailableVideos![0])"
        let downloadURL: NSURL = NSURL(string: stringURL)!
        let request: NSURLRequest = NSURLRequest(URL: downloadURL)
        let downloadTask: NSURLSessionDownloadTask = self.session!.downloadTaskWithRequest(request)
        downloadTask.resume()

    }
    
    func GetURLTimer(timer: NSTimer) {
        dispatch_async(dispatch_get_main_queue(), {() -> Void in
            //VideosDownloadedRef.text = String(format: "%u", filesDownloaded)
            //update UI with video downloaded count
        })
    }
    
    
    func getSession() -> NSURLSession {
        /*
         Using dispatch_once here ensures that multiple background sessions with the same identifier are not created in this instance of the application. If you want to support multiple background sessions within a single process, you should create each session with its own identifier.
         */
        var session: NSURLSession? = nil
        var onceToken: dispatch_once_t = 0
        dispatch_once(&onceToken, {() -> Void in
            let configuration: NSURLSessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier("com.example.apple-samplecode.SimpleBackgroundTransfer.BackgroundSession")
            session = NSURLSession(configuration: configuration, delegate: self, delegateQueue: nil)
            //session = NSURLSession.sessionWithConfiguration(configuration, delegate: self, delegateQueue: nil)
        })
        return session!
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL downloadURL: NSURL) {
        filesDownloaded += 1
        //get the videoURL
        let tempFilePath: String = downloadURL.path!
        let error: NSError? = nil
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory: String = paths[0] as! String
        // Get documents folder
        let dataPath: String = NSURL(fileURLWithPath: documentsDirectory).URLByAppendingPathComponent(GlobalVars.DocumentVideoDirectory).path!
        //NSString *newPath = [[tempFilePath stringByDeletingLastPathComponent] stringByAppendingPathComponent:AvailableVideos[0]];
        let newPath: String = NSURL(fileURLWithPath: dataPath).URLByAppendingPathComponent(AvailableVideos![0]).path!
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        
        if !fileManager.fileExistsAtPath(dataPath) {
            do {
                try fileManager.createDirectoryAtPath(dataPath, withIntermediateDirectories: false, attributes: nil)
            }
            catch {
            }
        }
        
        //Create folder
        if fileManager.fileExistsAtPath(newPath) == true {
            do {
                try fileManager.removeItemAtPath(newPath)
            }
            catch {
            }
        }
        
        do {
            try fileManager.copyItemAtPath(tempFilePath, toPath: newPath)
        }
        catch {
        }

        //[self videoSaved2:newPath error:error contextInfo:(__bridge void *)(AvailableVideos[0])];
        //self.videoSaved2(newPath, error: error!, contextInfo: AvailableVideos![0])
        /*if ( UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(newPath))
         // Copy it to the camera roll.
         UISaveVideoAtPathToSavedPhotosAlbum(newPath, self, @selector(videoSaved:didFinishSavingWithError:contextInfo:), (__bridge void *)(AvailableVideos[0]));
         
         else
         {
         [self ErrorOnDownloadOrSave];
         return;
         //        NSString *s = [[NSString alloc]initWithFormat:@"Can't save: %@ to camera roll.\n\r", AvailableVideos[0]];
         //        dispatch_async(dispatch_get_main_queue(), ^{
         //            DownloadTextRef.text = [DownloadTextRef.text stringByAppendingString:s];
         //        });
         }*/
        

        print("Downloaded video: \(AvailableVideos![0])")
        serverUtility.appendToVideoLogFile(AvailableVideos![0])
        AvailableVideos!.removeAtIndex(0)
        dispatch_async(dispatch_get_main_queue(), {() -> Void in
             self.BuildVideoCollection()
        })

        
        // Keep going
        if AvailableVideos!.count > 0 {
            //  we have popped the videos to download from the array. AvailableVideos[0] is always the next one to download.
            let stringURL: String = "http://\(GlobalVars.MyServerAddress)/\(AvailableVideos![0])"
            let downloadURL: NSURL = NSURL(string: stringURL)!
            let request: NSURLRequest = NSURLRequest(URL: downloadURL)
            let downloadTask: NSURLSessionDownloadTask = self.session!.downloadTaskWithRequest(request)
            downloadTask.resume()
        }
        else {
            // all is well
            timr!.invalidate()
            timr = nil
            //VideosAvailableRef.text = ""
            // show user result.
            /*var downloadedStr: "%i videos have been downloaded to your Camera Roll", CInteger(VideosDownloadedRef.text!)!)
            var alertController: UIAlertController = UIAlertController(title: "SCS", message: downloadedStr, preferredStyle: .Alert)
            var okAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("OK", "OK action"), style: .Default, handler: {(action: UIAlertAction) -> Void in
                alertController.dismissViewControllerAnimated(true, completion: { _ in })
                VideosDownloadedRef.text = ""
                // zero out at start.
            })
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: { _ in })*/
            
            //end progress spinner
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                self.BuildVideoCollection()
                self.activity.stopAnimating()
            })
        }


        //clean up of temp file downloaded
        do {
            try fileManager.removeItemAtURL(downloadURL)
        }
        catch {
        }
        
        if error != nil {
            // need alert
            print("Error \(error!)")
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                //DownloadTextRef.text = DownloadTextRef.text!.stringByAppendingString(s)
            })
        }
    }
    
    private func onSelectModeChange() {
        btnCancel.hidden = !isSelectMode
        btnDelete.hidden = !isSelectMode
        btnRefresh.hidden = isSelectMode
            
        BuildVideoCollection()
    }
    
    private func BuildVideoCollection() {
        videoCollection.removeAll()
        
        guard let videoArray = FileUtility.getAllFilesAndFoldersInDocumentVideoDirectory() else {
            print("no videos!")
            btnSelect.hidden = true
            return
        }
        
        if videoArray.count <= 0 {
            print("no videos!")
            btnSelect.hidden = true
            return
        }
        
        btnSelect.hidden = false
        
        for nsUrl in videoArray {
            let temp = VideoModel(VideoUrl: nsUrl, VideoTitle: "Video", VideoThumbNail:generateThumbImage(nsUrl))
            videoCollection.append(temp)
        }
        
    }
    
    func generateThumbImage(url : NSURL) -> UIImage{
        let asset : AVAsset = AVAsset(URL: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time:CMTime = CMTimeMake(1, 30)
        var img:CGImageRef?
        
        do {
            try img = assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
        } catch {
            print("not able to create image!")
            return UIImage(imageLiteral: "play-btn-thumbnail")
        }
        
        return UIImage(CGImage: img!)
    }

    // MARK: - Navigation
    let liveViewDetailSegue = "showLiveViewVideoPlayerSeque"
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if let ident = identifier {
            if ident == liveViewDetailSegue {
                if isSelectMode {
                    let cell = sender as! LiveViewCollectionViewCell
                    cell.toggleSelected()
                    return false
                }
            }
        }
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == liveViewDetailSegue {
            
            if let destination = segue.destinationViewController as? LiveViewPlayerViewController {
                let cell = sender as! LiveViewCollectionViewCell
                
                if let indexPath = collectionView!.indexPathForCell(cell) {
                    destination.videoModel = videoCollection[indexPath.row]
                }
            }
        }
    }


    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return false
     }
     
     override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
     return false
     }
     
     override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
     
     }
     */

}
