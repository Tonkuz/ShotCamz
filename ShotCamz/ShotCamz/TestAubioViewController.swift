//
//  TestAubioViewController.swift
//  ShotCamz
//
//  Created by Brian Ogden on 7/24/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import UIKit

class TestAubioViewController: UIViewController {

    let sampleAudioPath = FileUtility.getPathToAudioSampleFile()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //var fvect = fvec_t(length: 1024, data: &audioBuffer)
        //let source = new_aubio_source(, 0, 512) //COpaquePointer(audioBuffer.mData)
        
        // Do any additional setup after loading the view.
        
        prepareAubio()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func prepareAubio() {
        let samples = new_fvec(512)
        var total_frames : uint_t = 0
        let out_onset = new_fvec (1);
        var read : uint_t = 0

        guard let method = "default".cStringUsingEncoding(NSUTF8StringEncoding) else {
            print("UTF8 aka cString conversion failue")
            return
        }
        
        guard let cPath = sampleAudioPath.cStringUsingEncoding(NSUTF8StringEncoding) else {
            print("could not convert String to char_t aka Int8")
            return
        }

        
        let source = new_aubio_source(UnsafeMutablePointer<Int8>(cPath), 0, 512)
        let samplerate = aubio_source_get_samplerate(source)
        let aubioOnset:COpaquePointer = new_aubio_onset(UnsafeMutablePointer<Int8>(method), 512, 512, UInt32(samplerate))
        let testThres:smpl_t = 2.5
        
        
        //set threshold
        aubio_onset_set_threshold(aubioOnset, testThres)
        
        var onsetCount = 0
        while true {
            aubio_source_do(source, samples, &read)
            aubio_onset_do(aubioOnset, samples, out_onset)
            
            if (fvec_get_sample(out_onset, 0) != 0) {
                print(String(format: ">>> %.2f", aubio_onset_get_last_s(aubioOnset)))
                onsetCount += 1
            }
            
            total_frames += read
            
            if (read < 512) {
                break
            }
        }
        
        print("done, total onsetCount: \(onsetCount)")
        
        del_fvec(out_onset)
        del_aubio_onset(aubioOnset)
        del_aubio_source(source)
        del_fvec(samples)
    }
    
    private func testAubio() {
        
        //basic test that aubio works
        let a = new_fvec(512)
        fvec_ones(a)
        fvec_print(a)
        var method = "default"
        let b = new_aubio_tempo(method, 512, 256, 44100)
        let c = new_aubio_pitch("default", 512, 256, 32000)
        
        aubio_tempo_get_bpm(b)
        del_aubio_tempo(b)
        del_aubio_pitch(c)
        del_fvec(a)
        //end basic test of aubio
        
        /*let testThres:smpl_t = 2.5
         let ret:uint_t = (aubio_onset_set_threshold(aubioOnset, testThres))
         let param:smpl_t = (aubio_onset_get_threshold(aubioOnset))*/
        
        //print("return from aubio_onset_set_threshold: \(ret)")
        //uint_t aubio_onset_set_a_parameter (aubio_something_t *t, smpl_t a_parameter);
        
        //print(param)
        
        //new_aubio_source(UnsafeMutablePointer(), 44100, 512)
        
        /*
         let path : String = "/path/to/soundfile.wav"
         let t = path.withCString { cstr in
         new_aubio_source(UnsafeMutablePointer(cstr), 44100, 512)
         }
         /// do stuff with t, and later delete it
         del_aubio_source(t)
         */
        
        /*let a = new_fvec(512)
         fvec_ones(a)
         fvec_print(a)
         var method = "default"
         let b = new_aubio_tempo(method, 512, 256, 44100)
         let c = new_aubio_pitch("default", 512, 256, 32000)
         
         aubio_tempo_get_bpm(b)
         del_aubio_tempo(b)
         del_aubio_pitch(c)
         del_fvec(a)*/
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
