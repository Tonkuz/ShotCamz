//
//  VideoModel.swift
//  SCS
//
//  Created by Brian Ogden on 6/16/16.
//  Copyright © 2016 swiftiostutorials.com. All rights reserved.
//

import Foundation
import UIKit

struct VideoModel
{
    let VideoUrl: NSURL
    let VideoTitle: String
    let VideoThumbNail: UIImage
    //let VideoFilename: String
}