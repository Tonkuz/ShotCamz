//
//  MainTabBarViewController.swift
//  ShotCamz
//
//  Created by Brian Ogden on 6/17/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.barTintColor = GlobalUIColors.UIColorBackgroundTabBarMenu
        self.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName : GlobalUIColors.UIColorDefautFontColor], forState: UIControlState.Normal)
        self.tabBar.tintColor = GlobalUIColors.UIColorDefautFontColor
        
        //self.edgesForExtendedLayout = UIRectEdge.None
        //self.automaticallyAdjustsScrollViewInsets = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
