/*
 Copyright (C) 2015 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 Application preview view.
 */
import UIKit
import AVFoundation

class AAPLPreviewView: UIView {
    
    override class func layerClass() -> AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
    
    func session() -> AVCaptureSession {
        let previewLayer: AVCaptureVideoPreviewLayer = (self.layer as! AVCaptureVideoPreviewLayer)
        return previewLayer.session
    }
    
    func setSession(session: AVCaptureSession) {
        let previewLayer: AVCaptureVideoPreviewLayer = (self.layer as! AVCaptureVideoPreviewLayer)
        previewLayer.session = session
    }
}