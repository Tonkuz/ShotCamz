//
//  Camera.m
//  ShotCamz
//
//  Created by Tony Thomas on 26/08/2016.
//  Copyright © 2016 SCS. All rights reserved.
//

#import "Camera.h"
#import "MovieWriter.h"
#import "EZAudio/EZAudio.h"
@import AVFoundation;
@import CoreMotion;
@import UIKit;
@interface Camera() <AVCaptureVideoDataOutputSampleBufferDelegate>{
    
}
@property(nonatomic, strong)    AVCaptureSession* captureSession;
@property(nonatomic, strong)    AVCaptureDevice* inputCamera;
@property(nonatomic, strong)    NSMutableDictionary* formatsDictionary;
@property(nonatomic, strong)    NSMutableArray* frameRateList;
@property(nonatomic, strong)    MovieWriter* movieWriter;
@property(nonatomic)    int maxVideoHeight;
@property(nonatomic)    int maxFramerateForCamera;
@property(nonatomic)    dispatch_queue_t cameraProcessingQueue, audioProcessingQueue;
@property(nonatomic)    AVCaptureDeviceInput* videoInput;
@property(nonatomic)    AVCaptureVideoDataOutput* videoOutput;
@property (nonatomic)   AVCaptureConnection* avCaptureVideoConnection;
@property (strong, nonatomic) CMMotionManager *motionManager;

@end
@implementation Camera
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.movieWriter = [[MovieWriter alloc]init];
        self.motionManager = [[CMMotionManager alloc] init];
        self.motionManager.deviceMotionUpdateInterval = 0.2; // 10 Hz
        [self.motionManager startDeviceMotionUpdates];
    }
    return self;
}
-(void)setupCameraWithCompletionHandler:(void(^)(BOOL, AVCaptureSession*)) handler{
    
    NSError* error = nil;
    self.captureSession = [[AVCaptureSession alloc]init];
    self.inputCamera = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    self.videoInput =[AVCaptureDeviceInput deviceInputWithDevice:self.inputCamera error:&error];
    if (error) {
        handler(NO, nil);
        return;
    }
    if ([self.captureSession canAddInput:self.videoInput])
    {
        [self.captureSession addInput:self.videoInput];
    }
    
    self.videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [self.videoOutput setAlwaysDiscardsLateVideoFrames:YES];
    self.cameraProcessingQueue = dispatch_queue_create("shotCamz.camera.queue", DISPATCH_QUEUE_SERIAL);
    [self.videoOutput setSampleBufferDelegate:self queue:self.cameraProcessingQueue];
    if ([self.captureSession canAddOutput:self.videoOutput])
    {
        [self.captureSession addOutput:self.videoOutput];
    }else
    {
        NSLog(@"Couldn't add video output");
        handler(NO, nil);
        return;
    }
    [self.videoOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];

    [self enumerateResolutionAndFrameRates];
    
    self.avCaptureVideoConnection = [self.videoOutput connectionWithMediaType:AVMediaTypeVideo];
    [self.inputCamera lockForConfiguration:&error];
    AVCaptureDeviceFormat* format = [self.formatsDictionary objectForKey:@"720"];
    self.inputCamera.activeFormat = format;
    self.inputCamera .activeVideoMinFrameDuration = CMTimeMake(1, self.maxFramerateForCamera);
    self.inputCamera .activeVideoMaxFrameDuration = CMTimeMake(1, self.maxFramerateForCamera);
    [self setVideoOrientationForRaw];
    [self.inputCamera unlockForConfiguration];
    

    handler(YES, self.captureSession);
}
-(void)enumerateResolutionAndFrameRates{
    
    
    BOOL frontCamera = self.inputCamera.position== AVCaptureDevicePositionFront;
    //   [self.camController animateAlertForDuration:1 alertMessage:@"Wait.."];
    
    AVCaptureDevice *device = self.inputCamera;
    Float64 maxFrameRate = 0;
    self.formatsDictionary = [NSMutableDictionary new];
    self.frameRateList  = [NSMutableArray new];
    
    [self.frameRateList addObject:@"30 FPS"];
    int maxResolution = 720;
    NSArray* deviceFormats =[device formats];
    for ( AVCaptureDeviceFormat *format in  deviceFormats) {
        CMVideoDimensions dims = CMVideoFormatDescriptionGetDimensions([format formatDescription]);
        NSLog(@"%d %d Frame Rate", dims.width, dims.height);
        for ( AVFrameRateRange *range in format.videoSupportedFrameRateRanges ) {
            
            int nFR = range.maxFrameRate;
            
            if (frontCamera) {
                
                if (dims.height>720) {
                    continue;
                }
            }
            int nPrevFR = 0;
            AVCaptureDeviceFormat *prevFormat = [self.formatsDictionary objectForKey:[NSString stringWithFormat:@"%d",(int)dims.height]];
            if (prevFormat) {
                
                for (AVFrameRateRange *rangePrev in prevFormat.videoSupportedFrameRateRanges ) {
                    nPrevFR =rangePrev.maxFrameRate;
                  }
            }
            if (nPrevFR<nFR) {
                
                [self.formatsDictionary setObject:format forKey:[NSString stringWithFormat:@"%d",(int)dims.height]];
                NSLog(@"%d %d Frame Rate %d", dims.width, dims.height, nFR);
                self.maxVideoHeight = maxResolution = dims.height;
            }
            if (range.maxFrameRate>maxFrameRate) {
                
                maxFrameRate = range.maxFrameRate;
                
                /* if (maxFrameRate>30) {
                 
                 int nTimes = maxFrameRate/30;
                 for (int i=0; i<nTimes; i++) {
                 
                 [self.frameRateList addObject:[NSString stringWithFormat:@"%d FPS", (int)i*30]];
                 }
                 }*/
            }
            
        }
    }
    NSArray* array = [self.formatsDictionary allKeys];
    NSLog(@"%@",array);
    self.maxFramerateForCamera = maxFrameRate;
    NSLog(@"Max width %d max frame rate %d",self.maxVideoHeight,self.maxFramerateForCamera);
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"deviceEnumerated"
     object:self];
    
    
}
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if(self.recordingStarted){
        [self.movieWriter writeSample:sampleBuffer];
    }

}
-(void)startRecording{
    self.recordingStarted = YES;
}
-(void)setVideoOrientationForRaw{
    
    UIDeviceOrientation orientation =  [self realDeviceOrientation];
    
    
    if (self.inputCamera.position==AVCaptureDevicePositionBack) {
        
        switch (orientation) {
            case UIDeviceOrientationLandscapeLeft:
                [self.avCaptureVideoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
                self.previewImageOrientation = UIInterfaceOrientationPortrait;
                break;
            case UIDeviceOrientationLandscapeRight:
                [self.avCaptureVideoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                self.previewImageOrientation = UIInterfaceOrientationPortraitUpsideDown;
                break;
            case UIDeviceOrientationPortrait:
                [self.avCaptureVideoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                self.previewImageOrientation = UIInterfaceOrientationLandscapeRight;
                break;
            case UIDeviceOrientationPortraitUpsideDown:
                [self.avCaptureVideoConnection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
                self.previewImageOrientation = UIInterfaceOrientationLandscapeRight;
                break;
            case UIDeviceOrientationFaceUp:
                [self.avCaptureVideoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                self.previewImageOrientation = UIInterfaceOrientationLandscapeRight;
                break;
            case UIDeviceOrientationFaceDown:
                [self.avCaptureVideoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                self.previewImageOrientation = UIInterfaceOrientationLandscapeRight;
                break;
            default:
                [self.avCaptureVideoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                self.previewImageOrientation = UIInterfaceOrientationLandscapeRight;
                break;
        }
    }
 }
- (UIDeviceOrientation)realDeviceOrientation
{
    CMDeviceMotion *deviceMotion = self.motionManager.deviceMotion;
    if (!deviceMotion) {
        
        return UIDeviceOrientationPortrait;
    }
    double x = deviceMotion.gravity.x;
    double y = deviceMotion.gravity.y;
    
    if (fabs(y) >= fabs(x))
    {
        if (y >= 0)
            return UIDeviceOrientationPortraitUpsideDown;
        else
            return UIDeviceOrientationPortrait;
    }
    else
    {
        if (x >= 0)
            return UIDeviceOrientationLandscapeRight;
        else
            return UIDeviceOrientationLandscapeLeft;
    }
}

@end
