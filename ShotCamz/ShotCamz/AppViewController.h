//
//  AppViewController.h
//  CustomTabBar
//
//  Created by Brian Frohlich on 10/4/15.
//  Copyright © 2015 swiftiostutorials.com. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UIViewControllerPlus.h"
#import "Common.h"

@interface AppViewController : UIViewController<UITextFieldDelegate, NSStreamDelegate, NSURLConnectionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverPresentationControllerDelegate>
{
    NSMutableArray *configArray;
    NSOutputStream	*outputStream;
    NSInputStream	*inputStream;
    
    NSString *str;
    NSURLConnection *connection1;
    NSURLConnection *connection1_stream;
//    NSURLConnection *connection2;
//    NSURLConnection *connection2_steam;
//    NSURLConnection *connection3;
//    NSURLConnection *connection3_steam;
    
    NSURL *yourURL;
    
    UInt32  sizeOfJPEG_1;
    UInt32  sizeOfJPEGPad_1;
    NSMutableData  *jpegBytes_1;
    
//    UInt32  sizeOfJPEG_2;
//    UInt32  sizeOfJPEGPad_2;
//    NSMutableData  *jpegBytes_2;
//    
//    UInt32  sizeOfJPEG_3;
//    UInt32  sizeOfJPEGPad_3;
//    NSMutableData  *jpegBytes_3;
    
    NSTimer *timr;
    
    IBOutlet UITextField *RFIDTag;
    NSString *myRFIDTag;
};

@property (unsafe_unretained, nonatomic) IBOutlet UITextView *ConfigurationText;
@property (weak, nonatomic) IBOutlet UIButton *SaveButtonRef;

@property (weak, nonatomic) IBOutlet UISwitch *StartPreviewButtonRef;
@property (weak, nonatomic) IBOutlet UIButton *CancelButtonRef;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *DelayText;
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *Waiting;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *LengthText;

@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;


@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *Preview1Ref;


@property (weak, nonatomic) IBOutlet UIButton *StartButtonRef;
@property (weak, nonatomic) IBOutlet UIButton *DeleteAllButtonRef;

@property (weak, nonatomic) IBOutlet UITextField *VideosAvailableRef;
@property (weak, nonatomic) IBOutlet UITextField *VideosDownloadedRef;
@property (weak, nonatomic) IBOutlet UIButton *DeleteAllFromServerRef;

@property (weak, nonatomic) IBOutlet UITextView *DownloadTextRef;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SelectCameraRef;
@property (weak, nonatomic) IBOutlet UILabel *CameraSelectLabelRef;
@property (weak, nonatomic) IBOutlet UIView *previewView;

//@property(nonatomic) NSString *myRFIDTag;
@property (nonatomic) NSURLSession *session;


@property (strong, nonatomic) NSMutableArray * allowedBarcodeTypes;
@property (weak, nonatomic) IBOutlet UIButton *CancelScanButton;

@property (weak, nonatomic) IBOutlet UIImageView *ScanBackground;
@property (weak, nonatomic) IBOutlet UIView *PictureView;

@end
