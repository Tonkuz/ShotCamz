//
//  AvRecorderViewController.swift
//  ShotCamz
//
//  Created by Brian Ogden on 7/18/16.
//  Copyright © 2016 SCS. All rights reserved.
//

import UIKit
import AVFoundation
import CoreGraphics
import CoreVideo
import CoreMedia
import CoreAudio


class BackupAvRecorderViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    
    var captureSession: AVCaptureSession!
    //var captureMovieFileOutput: AVCaptureMovieFileOutput!
    //var test: AVCaptureAudioChannel!
    var imageView:UIImageView!
    var customLayer:CALayer!
    var prevLayer:AVCaptureVideoPreviewLayer!
    let samplingFrequency = Int32(30)
    var aubioOnset:COpaquePointer? = nil
    //var assetWriter: AVAssetWriter!
    //var assetWriterInput: AVAssetWriterInput!
    let pathToSoundSample = FileUtility.getPathToAudioSampleFile()
    let useAacAudtio = false
    //var testAudioRecorder: AVAudioRecorder! //http://stackoverflow.com/questions/12135687/record-audio-file-and-save-locally-on-iphone
    var assetWriterReady = false
    //var testAudioSession: AVAudioSession! //http://stackoverflow.com/questions/34059481/ios-avaudiorecorder-record-without-stopping-background-audio
    var audioPlayer: AVAudioPlayer!
    var onsetCount = 0
    let testThres:smpl_t = 0.03
    let nsMutableData: NSMutableData = NSMutableData()
    var sampleRate:UInt32!
    var bufferSize:UInt32!
    let useTimerAndNSMutableData = true
    
    /*var acl = AudioChannelLayout()
     bzero(&acl, sizeof(acl.dynamicType))
     acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono//kAudioChannelLayoutTag_Stereo
     
     let audioOutputSettings = [
     AVFormatIDKey : Int(kAudioFormatAppleLossless),
     //AVFormatIDKey : Int(kAudioFormatLinearPCM),
     AVEncoderBitDepthHintKey : Int(16),
     AVSampleRateKey : Int(44100.0),
     AVNumberOfChannelsKey : Int(1),
     AVChannelLayoutKey : NSData(bytes: &acl, length: sizeof(acl.dynamicType))
     ]*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if FileUtility.fileExistsAtPath(pathToSoundSample) {
            print("sample file exists")
            FileUtility.deleteFileByNsurl(NSURL(fileURLWithPath: pathToSoundSample))
        }
        setupCapture()
        
        if useTimerAndNSMutableData {
            //create timer for sampling audio
            NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(timerFiredPrepareForAubioOnsetSample), userInfo: nil, repeats: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        coordinator.animateAlongsideTransition({ (context) -> Void in
            
            }, completion: { (context) -> Void in
                
        })
    }
    
    override func viewWillLayoutSubviews() {
        prevLayer.frame = self.view.bounds
        
        if prevLayer.connection.supportsVideoOrientation {
            prevLayer.connection.videoOrientation = MediaUtility.interfaceOrientationToVideoOrientation(UIApplication.sharedApplication().statusBarOrientation)
        }
    }
    
    func timerFiredPrepareForAubioOnsetSample() {
        if nsMutableData.length <= 0 {
            return
        }
        
        
    }
    
    func setupCapture() {
        let captureDeviceVideo: AVCaptureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        let captureDeviceAudio: AVCaptureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeAudio)
        var captureVideoInput: AVCaptureDeviceInput
        var captureAudioInput: AVCaptureDeviceInput
        
        //video setup
        if captureDeviceVideo.isTorchModeSupported(.On) {
            try! captureDeviceVideo.lockForConfiguration()
            
            /*if captureDeviceVideo.position == AVCaptureDevicePosition.Front {
             captureDeviceVideo.position == AVCaptureDevicePosition.Back
             }*/
            
            //configure frame rate
            /*We specify a minimum duration for each frame (play with this settings to avoid having too many frames waiting
             in the queue because it can cause memory issues). It is similar to the inverse of the maximum framerate.
             In this example we set a min frame duration of 1/10 seconds so a maximum framerate of 10fps. We say that
             we are not able to process more than 10 frames per second.*/
            captureDeviceVideo.activeVideoMaxFrameDuration = CMTimeMake(1, samplingFrequency)
            captureDeviceVideo.activeVideoMinFrameDuration = CMTimeMake(1, samplingFrequency)
            captureDeviceVideo.unlockForConfiguration()
        }
        
        //try and create audio and video inputs
        do {
            try captureVideoInput = AVCaptureDeviceInput(device: captureDeviceVideo)
            try captureAudioInput = AVCaptureDeviceInput(device: captureDeviceAudio)
            
        } catch {
            print("cannot record")
            return
        }
        
        /*setup the output*/
        let captureVideoDataOutput: AVCaptureVideoDataOutput = AVCaptureVideoDataOutput()
        let captureAudioDataOutput: AVCaptureAudioDataOutput = AVCaptureAudioDataOutput()
        
        /*While a frame is processes in -captureVideoDataOutput:didOutputSampleBuffer:fromConnection: delegate methods no other frames are added in the queue.
         If you don't want this behaviour set the property to false */
        captureVideoDataOutput.alwaysDiscardsLateVideoFrames = true
        
        // Set the video output to store frame in BGRA (It is supposed to be faster)
        let videoSettings: [NSObject : AnyObject] = [kCVPixelBufferPixelFormatTypeKey:Int(kCVPixelFormatType_32BGRA)]
        
        captureVideoDataOutput.videoSettings = videoSettings
        
        /*And we create a capture session*/
        captureSession = AVCaptureSession()
        
        //and configure session
        //	AVCaptureSessionPresetHigh - Highest recording quality (varies per device)
        //	AVCaptureSessionPresetMedium - Suitable for WiFi sharing (actual values may change)
        //	AVCaptureSessionPresetLow - Suitable for 3G sharing (actual values may change)
        //	AVCaptureSessionPreset640x480 - 640x480 VGA (check its supported before setting it)
        //	AVCaptureSessionPreset1280x720 - 1280x720 720p HD (check its supported before setting it)
        //	AVCaptureSessionPresetPhoto - Full photo resolution (not supported for video output)
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        /*We add audio/video input and output to session*/
        captureSession.addInput(captureVideoInput)
        captureSession.addInput(captureAudioInput)
        captureSession.addOutput(captureVideoDataOutput)
        captureSession.addOutput(captureAudioDataOutput)
        
        
        //ADD MOVIE FILE OUTPUT to session
        /*captureMovieFileOutput = AVCaptureMovieFileOutput()
         //  Float64 TotalSeconds = 60;          //Total seconds
         //  int32_t preferredTimeScale = CAPTURE_FRAMES_PER_SECOND; //Frames per second
         //  CMTime maxDuration = CMTimeMakeWithSeconds(TotalSeconds, preferredTimeScale);   //<<SET MAX DURATION
         //  captureMovieFileOutput_.maxRecordedDuration = maxDuration;
         
         //http://stackoverflow.com/questions/26768987/avcapturesession-audio-doesnt-work-for-long-videos
         //captureMovieFileOutput.movieFragmentInterval = kCMTimeInvalid //something to do with audio on videos longer than 12 seconds
         
         captureMovieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024
         //<<SET MIN FREE SPACE IN BYTES FOR RECORDING TO CONTINUE ON A VOLUME
         if captureSession.canAddOutput(captureMovieFileOutput) {
         captureSession.addOutput(captureMovieFileOutput)
         }*/
        
        //not sure if I need this or not, found on internet
        captureSession.commitConfiguration()
        
        
        /*We create a serial queue to handle the processing of our frames*/
        var queue: dispatch_queue_t
        queue = dispatch_queue_create("queue", DISPATCH_QUEUE_SERIAL)
        
        //setup delegate
        captureVideoDataOutput.setSampleBufferDelegate(self, queue: queue)
        captureAudioDataOutput.setSampleBufferDelegate(self, queue: queue)
        
        /*do {
         try assetWriter = AVAssetWriter(URL: NSURL(fileURLWithPath: pathToSoundSample), fileType: AVFileTypeCoreAudioFormat)
         } catch {
         print("failed to creat assetWriter: \(error)")
         }*/
        
        //Configure Audio Writer Input
        var acl = AudioChannelLayout()
        bzero(&acl, sizeof(acl.dynamicType))
        acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono//kAudioChannelLayoutTag_Stereo
        
        var audioOutputSettings: [String : AnyObject]
        if useAacAudtio {
            audioOutputSettings = [
                AVFormatIDKey : Int(kAudioFormatMPEG4AAC),
                AVNumberOfChannelsKey : Int(1),
                AVSampleRateKey : Int(44100.0),
                AVChannelLayoutKey : NSData(bytes: &acl, length: sizeof(acl.dynamicType)),
                AVEncoderBitRateKey : Int(64000)
            ]
        }
        else {
            audioOutputSettings = [
                AVFormatIDKey : Int(kAudioFormatAppleLossless),
                //AVFormatIDKey : Int(kAudioFormatLinearPCM),
                AVEncoderBitDepthHintKey : Int(16),
                AVSampleRateKey : Int(44100.0),
                AVNumberOfChannelsKey : Int(1),
                AVChannelLayoutKey : NSData(bytes: &acl, length: sizeof(acl.dynamicType))
            ]
        }
        
        //assetWriterInput = AVAssetWriterInput(mediaType: AVMediaTypeAudio, outputSettings: audioOutputSettings)
        
        //assetWriterInput.expectsMediaDataInRealTime = true
        
        /*assetWriterInput.requestMediaDataWhenReadyOnQueue(queue) {
         print("assetWriter ready")
         self.assetWriterReady = true
         }*/
        
        //assetWriterInput.addTrackAssociationWithTrackOfInput(<#T##input: AVAssetWriterInput##AVAssetWriterInput#>, type: <#T##String#>)
        /*if assetWriter.canAddInput(assetWriterInput) {
         assetWriter.addInput(assetWriterInput)
         }else{
         print("failed to add AvAssetWriterInput")
         }*/
        
        /*We add the Custom Layer (We need to change the orientation of the layer so that the video is displayed correctly)*/
        customLayer = CALayer()
        customLayer.frame = self.view.bounds
        customLayer.transform = CATransform3DRotate(CATransform3DIdentity, CGFloat(M_PI) / 2.0, 0, 0, 1)
        customLayer.contentsGravity = kCAGravityResizeAspectFill
        view.layer.addSublayer(self.customLayer)
        
        /*We add the imageView*/
        imageView = UIImageView()
        imageView.frame = CGRectMake(0, 0, 100, 100)
        view!.addSubview(self.imageView)
        
        /*We add the preview layer*/
        prevLayer = AVCaptureVideoPreviewLayer()
        prevLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        prevLayer.frame = CGRectMake(100, 0, 100, 100)
        prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        view.layer.addSublayer(self.prevLayer)
        
        /*We start the capture*/
        captureSession.startRunning()
        /*if !assetWriter.startWriting() {
         print("assetWriter did not start")
         }*/
        
    }
    
    // MARK: AVCaptureSession delegate
    
    func captureOutput(captureOutput: AVCaptureOutput, didOutputSampleBuffer sampleBuffer: CMSampleBufferRef, fromConnection connection: AVCaptureConnection) {
        
        /*if (captureOutput is AVCaptureAudioDataOutput) {
         prepareAudioBuffer(sampleBuffer)
         }
         
         if (captureOutput is AVCaptureVideoDataOutput) {
         displayVideo(sampleBuffer)
         }*/
        
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didDropSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        print("frame dropped")
    }
    
    
    func captureOutput(captureOutput: AVCaptureFileOutput, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL, fromConnections connections: [AnyObject], error: NSError?) {
        print("\(#function)")
        /*print("\(outputFileURL)")
         print("\(captureVideoDataOutput)")
         var documentsDirectory: String = SMFileManager.applicationDocumentsDirectory()
         filePathCapturedVideo_ = NSURL(fileURLWithPath: documentsDirectory).URLByAppendingPathComponent("captured-video.mp4").absoluteString
         self.delegate.videoCaptured(filePathCapturedVideo_)*/
    }
    
    /*
     //example of building NSMutableData from the audioBufferList
     //http://stackoverflow.com/questions/21233706/how-to-create-audiobuffer-audio-from-nsdata
     func captureOutput(captureOutput: AVCaptureOutput, didOutputSampleBuffer sampleBuffer: CMSampleBufferRef, fromConnection connection: AVCaptureConnection) {
     var audioBufferList: AudioBufferList
     var data: NSMutableData = NSMutableData.data()
     var blockBuffer: CMBlockBufferRef
     CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBuffer, nil, audioBufferList, sizeof(), nil, nil, 0, blockBuffer)
     for y in 0..<audioBufferList.mNumberBuffers {
     var audioBuffer: AudioBuffer = audioBufferList.mBuffers[y]
     var frame: Float32 = (audioBuffer.mData as! Float32)
     data.appendBytes(frame, length: audioBuffer.mDataByteSize)
     }
     CFRelease(blockBuffer)
     CFRelease(ref)
     var player: AVAudioPlayer = try! AVAudioPlayer(data: data)
     player.play()
     }*/
    
    func updateAudioLevels(timer: NSTimer) {
        /*var channelCount: Int = 0
         var decibels: Float = 0.0
         // Sum all of the average power levels and divide by the number of channels
         for connection: AVCaptureConnection in self.movieFileOutput().connections {
         for audioChannel: AVCaptureAudioChannel in connection.audioChannels() {
         decibels += audioChannel.averagePowerLevel()
         channelCount += 1
         }
         }
         decibels /= channelCount
         self.audioLevelMeter().floatValue = (pow(10.0, 0.05 * decibels) * 20.0)*/
    }
    
    /*func writeAudioSamplebuffer(recordedSampleBuffer: CMSampleBufferRef) {
     var backgroundSampleBuffer: CMSampleBufferRef = backgroundAudioTrackOutput.copyNextSampleBuffer()
     /* DO MAGIC HERE  */
     var resultSampleBuffer: CMSampleBufferRef = self.overlapBuffer(recordedSampleBuffer, withBackgroundBuffer: backgroundSampleBuffer)
     /* END MAGIC HERE */
     assetWriterAudioInput.appendSampleBuffer(resultSampleBuffer)
     }*/
    
    private func sampleAudioForOnsets(data: UnsafeMutablePointer<smpl_t>, length: UInt32) {
        print("\(#function)")
        
        //let samples = new_fvec(512)
        var total_frames : uint_t = 0
        let out_onset = new_fvec (1)
        var read : uint_t = 0
        
        if aubioOnset == nil {
            let method = ("default" as NSString).UTF8String
            aubioOnset = new_aubio_onset(UnsafeMutablePointer<Int8>(method), bufferSize, 512, UInt32(sampleRate))
            aubio_onset_set_threshold(aubioOnset!, testThres)
        }
        
        var sample: fvec_t = fvec_t(length: length, data: data)
        
        
        while true {
            //aubio_source_do(COpaquePointer(source), samples, &read)
            aubio_onset_do(aubioOnset!, &sample, out_onset)
            
            if (fvec_get_sample(out_onset, 0) != 0) {
                print(String(format: ">>> %.2f", aubio_onset_get_last_s(aubioOnset!)))
                onsetCount += 1
            }
            
            total_frames += read
            
            //always will break the first iteration, only reason for while loop is to demonstate the normal use of aubio using aubio_source_do to read
            if (read < 512) {
                break
            }
        }
        
        print("done, total onsetCount: \(onsetCount)")
        
        if onsetCount > 1 {
            print("we are getting onsets")
        }
    }
    
    // MARK: - Private Helpers
    
    private func prepareAudioBuffer(sampleBuffer: CMSampleBufferRef) {
        /*if !assetWriterReady {
         return
         }*/
        
        let numSamplesInBuffer = CMSampleBufferGetNumSamples(sampleBuffer)
        bufferSize = UInt32(CMSampleBufferGetTotalSampleSize(sampleBuffer))
        var blockBuffer:CMBlockBufferRef? = nil
        var audioBufferList = AudioBufferList(mNumberBuffers: 1, mBuffers: AudioBuffer(mNumberChannels: 0, mDataByteSize: 0, mData: nil))
        var status:OSStatus
        let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)!
        let asbd = CMAudioFormatDescriptionGetStreamBasicDescription(formatDescription)
        sampleRate = UInt32(asbd.memory.mSampleRate)
        
        //print("formatDescription: \(formatDescription)")
        print("bufferSize: \(bufferSize)")
        print("numSamplesInBuffer: \(numSamplesInBuffer)")
        print("Sample Rate: \(sampleRate)")
        print("assetWriter.status: ")
        //MediaUtility.printAVAssetWriterStatus(assetWriter.status)
        
        /*if(assetWriterInput != nil && assetWriter.status == AVAssetWriterStatus.Writing){
         print("appendingSampleBuffer to assetWriterInput")
         assetWriterInput.appendSampleBuffer(sampleBuffer)
         }*/
        
        status = CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(
            sampleBuffer,
            nil,
            &audioBufferList,
            sizeof(audioBufferList.dynamicType), // instead of UInt(sizeof(audioBufferList.dynamicType))
            nil,
            nil,
            UInt32(kCMSampleBufferFlag_AudioBufferList_Assure16ByteAlignment),
            &blockBuffer
        )
        
        
        let audioBuffers = UnsafeBufferPointer<AudioBuffer>(start: &audioBufferList.mBuffers, count: Int(audioBufferList.mNumberBuffers))
        
        for audioBuffer in audioBuffers {
            
            //var fvect = fvec_t(length: 1024, data: &audioBuffer)
            //let source = new_aubio_source(, 0, 512) //COpaquePointer(audioBuffer.mData)
            
            let frame=UnsafePointer<Float32>(audioBuffer.mData)
            
            if useTimerAndNSMutableData {
                //NSDATA APPEND
                nsMutableData.appendBytes(frame, length: Int(audioBuffer.mDataByteSize))
            }else{
                //this never fails but there are never any onsets either, cannot tell if the audio sampling is just not long enough
                //or if the data really isn't valid data
                //smpl_t is a Float
                let data = UnsafeMutablePointer<smpl_t>(audioBuffer.mData)
                sampleAudioForOnsets(data, length: audioBuffer.mDataByteSize)
            }
            
        }
        
    }
    
    private func displayVideo(sampleBuffer: CMSampleBufferRef) {
        let imageBuffer: CVImageBufferRef = CMSampleBufferGetImageBuffer(sampleBuffer)!
        
        /*Lock the image buffer*/
        CVPixelBufferLockBaseAddress(imageBuffer, 0);
        
        /*Get information about the image*/
        let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)
        let bytesPerRow: size_t = CVPixelBufferGetBytesPerRow(imageBuffer)
        let width: size_t = CVPixelBufferGetWidth(imageBuffer)
        let height: size_t = CVPixelBufferGetHeight(imageBuffer)
        /*Create a CGImageRef from the CVImageBufferRef*/
        let colorSpace: CGColorSpaceRef = CGColorSpaceCreateDeviceRGB()!
        
        // Create a bitmap graphics context with the sample buffer data
        let newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, CGBitmapInfo.ByteOrder32Little.rawValue | CGImageAlphaInfo.PremultipliedFirst.rawValue)
        
        let newImage = CGBitmapContextCreateImage(newContext)
        
        /*We release some components*/
        //CGContextRelease(newContext)
        //CGColorSpaceRelease(colorSpace)
        
        /*We display the result on the custom layer. All the display stuff must be done in the main thread because
         UIKit is no thread safe, and as we are not in the main thread (remember we didn't use the main_queue)
         we use performSelectorOnMainThread to call our CALayer and tell it to display the CGImage.*/
        dispatch_sync(dispatch_get_main_queue(), {() -> Void in
            self.customLayer.contents = (newImage as! AnyObject)
        })
        /*We display the result on the image view (We need to change the orientation of the image so that the video is displayed correctly).
         Same thing as for the CALayer we are not in the main thread so ...*/
        let image = UIImage(CGImage: newImage!, scale: 1.0, orientation: .Up)
        /*We relase the CGImageRef*/
        // CGImageRelease(newImage)
        dispatch_sync(dispatch_get_main_queue(), {() -> Void in
            self.imageView!.image = image
        })
        /*We unlock the  image buffer*/
        CVPixelBufferUnlockBaseAddress(imageBuffer, 0)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
