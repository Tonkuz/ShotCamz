//
//  CameraViewController.m
//  ShotCamz
//
//  Created by Tony Thomas on 27/08/2016.
//  Copyright © 2016 SCS. All rights reserved.
//

#import "CameraViewController.h"
#import "Camera.h"
#import "MovieWriter.h"
@import AVFoundation;
@interface CameraViewController()
@property (weak, nonatomic) IBOutlet UIView *previewCanvas;
@property(nonatomic, strong) Camera* camera;

@property(nonatomic, strong) AVCaptureVideoPreviewLayer* preview;
@end
@implementation CameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.camera = [[Camera alloc]init];
    [self.camera setupCameraWithCompletionHandler:^(BOOL success, AVCaptureSession* session){
        if (success) {
            self.preview = [[AVCaptureVideoPreviewLayer alloc] initWithSession: session];
            [self.preview setBackgroundColor:[[UIColor blackColor] CGColor]];
            self.previewCanvas.frame = self.view.frame;
            CALayer *rootLayer = [self.previewCanvas layer];
            [rootLayer addSublayer:self.preview];
            [self.preview setFrame:[rootLayer bounds]];
            [self.preview setVideoGravity:AVLayerVideoGravityResizeAspectFill];
             self.preview.connection.videoOrientation = self.camera.previewImageOrientation;
            [session startRunning];
            
        }
    }];
}
- (IBAction)startRecording:(id)sender {
    
    
}

@end
