//
//  String.swift
//  SCS
//
//  Created by Brian Ogden on 6/16/16.
//  Copyright © 2016 swiftiostutorials.com. All rights reserved.
//

import Foundation

extension String {
    var length : Int {
        return self.characters.count
    }

    func stringByAppendingPathComponent(pathComponent: String) -> String {
        return (self as NSString).stringByAppendingPathComponent(pathComponent)
    }
}