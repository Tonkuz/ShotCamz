//
//  CoreUtility.swift
//  SCS
//
//  Created by Brian Ogden on 6/16/16.
//  Copyright © 2016 swiftiostutorials.com. All rights reserved.
//

import Foundation
import UIKit

struct FileUtility
{
    // TODO: Implement map filter correctly
    /*static func getMP4FilesInDocumentVideoDirectory() -> [NSURL]? {
        guard let directoryUrls = getAllFilesAndFoldersInDocumentVideoDirectory() else {
            return nil
        }
        
        /*var testArray = [NSURL]()
        
        testArray.append(NSURL(fileURLWithPath: "/Users/sweetog/Library/Developer/CoreSimulator/Devices/3E97A6A8-A42B-45D6-AA55-1EF537DDC5D2/data/Containers/Data/Application/C0FF1C11-8614-4B2A-83A7-CA4DB13EA951/Documents/Videos/E2004BA5F92F92B0018E6E49_1_16-06-2016-22-29-31.mp4"))
        
        testArray.append(NSURL(fileURLWithPath: "/Users/sweetog/Library/Developer/CoreSimulator/Devices/3E97A6A8-A42B-45D6-AA55-1EF537DDC5D2/data/Containers/Data/Application/C0FF1C11-8614-4B2A-83A7-CA4DB13EA951/Documents/Videos/E2004BA5F92F92B0018E6E49_1_16-06-2016-22-29-31.mp4"))*/
        
        let mp4Files = directoryUrls.filter{ $0.pathExtension == "mp4" }.map{
            NSURL(fileURLWithPath: $0.lastPathComponent!)
        }
        
        return mp4Files
        //print("MP3 FILES:\n" + mp3Files.description)
    }*/
    
    static func getPathInBundle(fileName: String, ofType: String) -> String? {
        return NSBundle.mainBundle().pathForResource(fileName, ofType: ofType)
    }
    
    static func deleteFileByNsurl(nsUrl: NSURL) {
        let fm = NSFileManager.defaultManager()
        do {
           try fm.removeItemAtURL(nsUrl)
        }
        catch let error1 as NSError {
            print("error deleteing file by NSURL: ")
            print(error1.description)
        }

    }
    
    static func getAllFilesAndFoldersInDocumentVideoDirectory() -> [NSURL]? {
        return getAllFilesAndFolders(inDirectory: .DocumentDirectory, subdirectory: GlobalVars.DocumentVideoDirectory)
    }

    static func getAllFilesAndFoldersInDocumentDirectory(inSubdirectory subdirectory:String?) -> [NSURL]? {
        return getAllFilesAndFolders(inDirectory: .DocumentDirectory, subdirectory: subdirectory)
    }
    
    static func getAllFilesAndFolders(inDirectory directory:NSSearchPathDirectory, subdirectory:String?) -> [NSURL]? {
        
        // Create load path
        if let loadPath = buildPathToDirectory(directory, subdirectory: subdirectory) {
            
            let videoDirectory = NSURL(fileURLWithPath: loadPath)
            //var array:[NSURL]? = nil
            
            let properties = [NSURLLocalizedNameKey,
                              NSURLCreationDateKey, NSURLLocalizedTypeDescriptionKey]
            
            if let urlArray = try? NSFileManager.defaultManager().contentsOfDirectoryAtURL(videoDirectory,includingPropertiesForKeys: properties, options:.SkipsHiddenFiles) {
                
                return urlArray.map { url -> (NSURL, NSTimeInterval) in
                    var lastModified : AnyObject?
                    _ = try? url.getResourceValue(&lastModified, forKey: NSURLContentModificationDateKey)
                    return (url, lastModified?.timeIntervalSinceReferenceDate ?? 0)
                    }
                    .sort({ $0.1 > $1.1 }) // sort descending modification dates
                    .map { $0.0 } // extract file names
                
            } else {
                return nil
            }

            
            /*do {
                array = try NSFileManager.defaultManager().contentsOfDirectoryAtURL(url, includingPropertiesForKeys: properties, options:NSDirectoryEnumerationOptions.SkipsHiddenFiles)
            }
            catch let error1 as NSError {
                print(error1.description)
            }
            
            return array*/
        }

        return nil
    }
    
    static func fileExistsAtPath(path: String) -> Bool {
        let fm = NSFileManager.defaultManager()
        return fm.fileExistsAtPath(path)
    }
    
    static func getPathToAudioSampleFile() -> String {
        if AppDelegate.UseMockExampleAudio {
            return getPathToBundledExampleAudioFile()
        }else{
            return getPathToAudioSampleFileHelper()
        }
        
    }
    
    static func buildPathToTempDirectory(fileName:String) -> String {
        let tempDirectory = NSTemporaryDirectory()
        return tempDirectory.stringByAppendingPathComponent(fileName)
    }
    
    // MARK: - Private Helpers
    
    private static func getPathToBundledExampleAudioFile() -> String {
        return getPathInBundle("example", ofType:"caf")!
    }
    
    private static func getPathToAudioSampleFileHelper() -> String{
        
        return FileUtility.buildPathToTempDirectory(GlobalVars.MySampleAudioFileNameAndExt)
        
        //using Documents/Audio directory, only did this to make sure things worked from tmp directory
        /*let fm = NSFileManager.defaultManager()
        var isDir : ObjCBool = true
        let directory = buildPathToDirectory(.DocumentDirectory, subdirectory: GlobalVars.DocumentAudioDirectory)!
        
        if !fm.fileExistsAtPath(directory, isDirectory: &isDir) {
            do {
                try fm.createDirectoryAtPath(directory, withIntermediateDirectories: false, attributes: nil)
            } catch {
                print("error creating Audio Sample directory")
            }
        }
        
        return directory.stringByAppendingPathComponent(GlobalVars.MySampleAudioFileNameAndExt)*/
    }
    
    private static func buildPathToDirectory(directory: NSSearchPathDirectory, subdirectory: String?) -> String? {
        let directoryString = NSSearchPathForDirectoriesInDomains(directory, .UserDomainMask, true)[0]
        var retVal: String?
        
        if(subdirectory != nil) {
           let newPath = directoryString.stringByAppendingPathComponent(subdirectory!)
            retVal = String(newPath)
        } else {
            retVal = directoryString
        }
        
        return retVal
    }
}